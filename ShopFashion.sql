create database shop_fashion;
use shop_fashion;
ALTER SCHEMA `shop_fashion`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_unicode_ci ;

CREATE TABLE cities(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null unique,
	name nvarchar(200) not null,
	type nvarchar(200) not null
);

CREATE TABLE districts(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null unique,
	name nvarchar(200) not null,
	type nvarchar(200) not null,
    city_id bigint not null,
    Foreign key(city_id) references cities(id)
);

CREATE TABLE wards(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50),
	name nvarchar(200),
	type nvarchar(200),
    district_id bigint,
    Foreign key(district_id) references districts(id)
);

CREATE TABLE roles(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(20) not null,
	name nvarchar(50) not null,
	level int not null,
    activity bit not null
);

INSERT INTO roles(code, name, level, activity) VALUES
('ROLE_ADMIN', 'Quản trị viên', 1, 1),
('ROLE_MANAGER', 'Quản lý', 2, 1),
('ROLE_EMPLOYEE', 'Nhân viên', 3, 1),
('ROLE_GUEST', 'Khách hàng', 4, 1);

CREATE TABLE accounts(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
	username varchar(50) not null,
	fullname nvarchar(200) not null,
	password varchar(200) not null,
    phone varchar(11),
	email varchar(200) not null,
    birthday date,
    gender enum('MALE', 'FEMALE', 'OTHER'),
	address text,
    city_id bigint,
    district_id bigint,
    ward_id bigint,
    activity bit not null,
    avatar varchar(200),
    deleted bit not null,
    deleted_by bigint,
    Foreign key(city_id) references cities(id),
    Foreign key(district_id) references districts(id),
    Foreign key(ward_id) references wards(id)
);

CREATE TABLE authorities(
	Id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
	account_id bigint not null,
	role_id bigint not null,
	Foreign key(account_id) references accounts(id),
	Foreign key(role_id) references roles(id)
);

CREATE TABLE verifications(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
	account_id bigint not null,
    password varchar(200) not null,
    code varchar(6) not null,
	expiry datetime not null,
    type int not null,
    activity bit not null,
	Foreign key(account_id) references accounts(id)
);

CREATE TABLE units(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null unique,
    name nvarchar(50) not null
);

CREATE TABLE category_groups(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null unique,
    name nvarchar(50) not null,
    level int not null,
    icon varchar(200) not null,
    activity bit not null,
    deleted bit not null,
    deleted_by bigint
);

CREATE TABLE categories(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null,
    name nvarchar(50) not null,
    group_id bigint not null,
    level int not null,
    view bit not null,
    activity bit not null,
    deleted bit not null,
    deleted_by bigint,
    Foreign key(group_id) references category_groups(id)
);

CREATE TABLE brands(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null,
    name nvarchar(200) not null,
    short_description text not null,
    description longtext not null,
    avatar varchar(200) not null,
    activity bit not null,
	deleted bit not null,
    deleted_by bigint
);

CREATE TABLE attribute_groups(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null,
    name nvarchar(200) not null,
    type varchar(200) not null,
    description longtext not null,
    activity bit not null,
	deleted bit not null,
    deleted_by bigint
);

CREATE TABLE attributes(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    attribute_group_id bigint not null,
    name nvarchar(200) not null,
    description longtext not null,
    value varchar(200) not null,
	activity bit not null,
	deleted bit not null,
    deleted_by bigint,
    Foreign key(attribute_group_id) references attribute_groups(id)
);

CREATE TABLE products(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    code varchar(50) not null,
    name nvarchar(500) not null,
    slug nvarchar(500) not null,
    qty bigint not null,
    price bigint not null,
    type varchar(50) not null,
    short_description text not null,
    description longtext not null,
    size_chart longtext,
    brand_id bigint not null,
    unit_id bigint not null,
    weight bigint not null,
    weight_unit varchar(50) not null,
    sold_count bigint not null,
    activity bit not null,
    created_at datetime not null,
	deleted bit not null,
    deleted_by bigint,
    Foreign key(brand_id) references brands(id),
    Foreign key(unit_id) references units(id)
);

CREATE TABLE product_categories(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    product_id bigint not null,
    category_id bigint not null,
	Foreign key(product_id) references products(id),
    Foreign key(category_id) references categories(id)
);

CREATE TABLE product_images(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(500) not null,
    product_id bigint not null,
    Foreign key(product_id) references products(id)
);

CREATE TABLE product_variants(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name text not null,
    price bigint not null,
    qty bigint not null,
    weight bigint not null,
    weight_unit varchar(50) not null,
    sold_count bigint not null,
    image varchar(200) not null,
    product_id bigint not null,
    activity bit not null,
	deleted bit not null,
    deleted_by bigint,
    Foreign key(product_id) references products(id)
);

CREATE TABLE product_attributes(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    product_variant_id bigint not null,
    attribute_id bigint not null,
    Foreign key(product_variant_id) references product_variants(id),
    Foreign key(attribute_id) references attributes(id)
);

CREATE TABLE price_histories(
	id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
	product_id bigint not null,
    product_variant_id bigint,
    price_old bigint not null,
    price bigint not null,
    created_at datetime not null,
    created_by bigint,
    Foreign key(product_id) references products(id),
    Foreign key(product_variant_id) references product_variants(id),
    Foreign key(created_by) references accounts(id)
);

select * from roles;
select * from cities;
select * from districts;
select * from wards;
select * from authorities;
select * from accounts;
select * from verifications;
select * from units;
select * from category_groups;
select * from categories;
select * from brands;
select * from attribute_groups;
select * from products;
select * from product_images;

SELECT * FROM products pr WHERE pr.deleted = false AND pr.deleted_by IS NULL ORDER BY pr.created_at DESC LIMIT 0, 4;

