package com.shopfashion.message;

import com.shopfashion.entity.Account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class ResponseMessageAccount {
	
	private String message;
	private Account account;
	
}
