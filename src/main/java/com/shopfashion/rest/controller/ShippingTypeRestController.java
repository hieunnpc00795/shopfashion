package com.shopfashion.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.entity.ShippingType;
import com.shopfashion.service.ShippingTypeService;
import com.shopfashion.validation.ShippingTypeCreateValidation;
import com.shopfashion.validation.ShippingTypeUpdateValidation;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/shipping-types")
public class ShippingTypeRestController {

	@Autowired
	ShippingTypeService shippingTypeService;
	
	@GetMapping()
	public List<ShippingType> getListShippingTypes(){
		return shippingTypeService.getListShippingTypes();
	}
	
	@PostMapping()
	public ResponseEntity<?> create(@RequestBody @Valid ShippingTypeCreateValidation shippingTypeCreateValidation){
		return shippingTypeService.create(shippingTypeCreateValidation);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody @Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation){
		return shippingTypeService.update(shippingTypeUpdateValidation);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> delete(@RequestBody @Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation){
		return shippingTypeService.delete(shippingTypeUpdateValidation);
		
	}
}
