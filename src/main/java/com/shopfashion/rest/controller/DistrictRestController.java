package com.shopfashion.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.entity.District;
import com.shopfashion.service.DistrictService;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/districts")
public class DistrictRestController {

	@Autowired
	DistrictService districtService;
	
	@GetMapping("/{cityId}")
	public List<District> getListDistrictsByCity(@PathVariable("cityId") Integer cityId){
		return districtService.getListDistrictsByCity(cityId);
	}
	
}
