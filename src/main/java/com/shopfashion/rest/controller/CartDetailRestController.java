package com.shopfashion.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.service.CartService;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/cart-details")
public class CartDetailRestController {

	@Autowired
	CartService cartService;
	
}
