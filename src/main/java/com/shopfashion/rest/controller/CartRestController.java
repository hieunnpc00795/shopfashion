package com.shopfashion.rest.controller;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.dto.CartRequest;
import com.shopfashion.service.CartService;
import com.shopfashion.validation.AddToCartValidation;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/carts")
public class CartRestController {

	@Autowired
	CartService cartService;
	
	@GetMapping("/{session-id}/{token}")
	public ResponseEntity<?> detail(@PathVariable("session-id") String session_id, @PathVariable("token") String token){
		return cartService.detail(session_id, token);
	}
	
	@PostMapping()
	public ResponseEntity<?> totalProductsInCart(@RequestBody @Valid CartRequest cartRequest){
		return cartService.totalProductsInCart(cartRequest);
	}
	
	@PostMapping("/add-to-cart")
	public ResponseEntity<?> addToCart(@RequestBody @Valid AddToCartValidation addToCartValidation){
		return cartService.addToCart(addToCartValidation);
	}
	
	@PostMapping("/merge-cart")
	public void mergeClientCartToUserCart(@RequestBody CartRequest cartRequest) {
		cartService.mergeClientCartToUserCart(cartRequest);
	}
	
}
