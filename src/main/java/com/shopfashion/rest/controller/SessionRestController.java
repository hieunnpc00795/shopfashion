package com.shopfashion.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.service.SessionService;

@CrossOrigin("*")
@RestController
@RequestMapping("/client/session")
public class SessionRestController {

	@Autowired
	SessionService sessionService;
	
	@GetMapping()
	public ResponseEntity<?> getSession(HttpServletRequest request) {
		return sessionService.getSession(request);
	}
	
}
