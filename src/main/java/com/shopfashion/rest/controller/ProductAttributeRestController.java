package com.shopfashion.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.dto.AttributeRequest;
import com.shopfashion.dto.AttributeResponse;
import com.shopfashion.entity.Attribute;
import com.shopfashion.service.ProductAttributeService;
import com.shopfashion.validation.GetVariantByAttributesValidation;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/product-attributes")
public class ProductAttributeRestController {

	@Autowired
	ProductAttributeService productAttributeService;
	
	@PostMapping("/{attID}/{level}")
	public AttributeResponse getListAttribute(@PathVariable("attID") Integer attID, @PathVariable("level") Integer level, 
			@RequestBody GetVariantByAttributesValidation getVariantByAttributesValidation){
		return productAttributeService.getListAttribute(attID, level, getVariantByAttributesValidation);
	}
}
