package com.shopfashion.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.entity.Ward;
import com.shopfashion.service.WardService;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/wards")
public class WardRestController {

	@Autowired
	WardService wardService;
	
	@GetMapping("/{districtId}")
	public List<Ward> getListWardsByDistrict(@PathVariable("districtId") Integer districtId){
		return wardService.getListWardsByDistrict(districtId);
	}
}
