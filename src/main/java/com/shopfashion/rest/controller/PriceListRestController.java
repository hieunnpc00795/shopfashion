package com.shopfashion.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopfashion.entity.PriceList;
import com.shopfashion.service.PriceListService;
import com.shopfashion.validation.PriceListCreateValidation;
import com.shopfashion.validation.PriceListUpdateValidation;

@CrossOrigin("*")
@RestController
@RequestMapping("/rest/price-lists")
public class PriceListRestController {

	@Autowired
	PriceListService priceListService;
	
	@GetMapping()
	public List<PriceList> findAll(){
		return priceListService.findAll();
	}
	
	@PostMapping()
	public ResponseEntity<?> create(@RequestBody @Valid PriceListCreateValidation priceListCreateValidation){
		return priceListService.create(priceListCreateValidation);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody @Valid PriceListUpdateValidation priceListUpdateValidation){
		return priceListService.update(priceListUpdateValidation);
	}
	
}
