package com.shopfashion.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopfashion.dao.ProductAttributeDAO;
import com.shopfashion.dao.ProductVariantDAO;
import com.shopfashion.entity.Attribute;
import com.shopfashion.entity.Category;
import com.shopfashion.entity.CategoryGroup;
import com.shopfashion.entity.Product;
import com.shopfashion.entity.ProductVariant;
import com.shopfashion.service.CategoryGroupService;
import com.shopfashion.service.CategoryService;
import com.shopfashion.service.ProductService;


@Controller
public class ProductController {

	@Autowired
	CategoryGroupService categoryGroupService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductAttributeDAO productAttributeDAO;
	
	@Autowired
	ProductVariantDAO productVariantDAO;
	
	@RequestMapping("/products/{slug}/{id}")
	public String product_detail(@PathVariable("id") String id, @PathVariable("slug") String slug, Model model) {
		
		List<CategoryGroup> category_groups = categoryGroupService.findAllCategoryGroup();
		model.addAttribute("category_groups", category_groups);
		
		List<Category> categories = categoryService.findAllCategory();
		model.addAttribute("categories", categories);
		
		String matches = "^[0-9]+$";
		if(!id.matches(matches)) {
			return "supports/pages_misc_error";
		}
		Integer proId = Integer.parseInt(id);
		Product product = productService.productDetail(proId, slug);
		if(product == null) {
			return "supports/pages_misc_error";
		}
		
		List<Attribute> attributes = productAttributeDAO.getListAtrributesByProduct(proId, 1);
		model.addAttribute("product", product);
		model.addAttribute("attributes", attributes);
		return "product/product_detail";
	}
	
}
