
package com.shopfashion.controller;

import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopfashion.dao.CategoryGroupDAO;
import com.shopfashion.entity.Category;
import com.shopfashion.entity.CategoryGroup;
import com.shopfashion.entity.Product;
import com.shopfashion.service.CategoryGroupService;
import com.shopfashion.service.CategoryService;
import com.shopfashion.service.ProductService;

@Controller
public class HomeController {

	@Autowired
	CategoryGroupService categoryGroupService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	ProductService productService;
	
	@RequestMapping({ "/admin", "/admin/home/index" })
	public String admin() {
		return "redirect:/admin/layout/index.html";
	}
	
	@RequestMapping("/home")
	public String index(Model model, HttpServletRequest request) {
		
		List<CategoryGroup> category_groups = categoryGroupService.findAllCategoryGroup();
		model.addAttribute("category_groups", category_groups);
		
		List<Category> categories = categoryService.findAllCategory();
		model.addAttribute("categories", categories);
		
		List<Product> new_products = productService.findNewProducts();
		model.addAttribute("new_products", new_products);
		
		return "home/home_page";
	}
	
	@RequestMapping("/signin")
	public String login() {
		return "authentications/login_page";
	}
	
	@RequestMapping("/register")
	public String register() {
		return "authentications/register_page";
	}
	
	@RequestMapping("/forgot-password")
	public String forgotPassword() {
		return "authentications/forgot_password_page";
	}
	
	@RequestMapping("/cart")
	public String cart() {
		return "cart/cart";
	}
	
	@RequestMapping("/cart-detail")
	public String cart_detail() {
		return "cart/cart_detail";
	}
	
	@RequestMapping("/product-detail")
	public String product_detail() {
		return "product/product_detail";
	}
	
	@RequestMapping("/post")
	public String post() {
		return "post/post";
	}
	
	@RequestMapping("/checkout")
	public String checkout() {
		return "checkout/success";
	}
	
	@RequestMapping("/list-product")
	public String list_product() {
		return "product/list_product";
	}
	
}
