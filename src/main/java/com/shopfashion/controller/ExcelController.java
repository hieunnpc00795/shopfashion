package com.shopfashion.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.common.CityExcelHelper;
import com.shopfashion.common.DistrictExcelHelper;
import com.shopfashion.common.UnitExcelHelper;
import com.shopfashion.common.WardExcelHelper;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.service.impl.CityExcelService;
import com.shopfashion.service.impl.DistrictExcelService;
import com.shopfashion.service.impl.UnitExcelService;
import com.shopfashion.service.impl.WardExcelService;

@CrossOrigin("http://localhost:8080")
@Controller
@RequestMapping("/api/excel")
public class ExcelController {
	
	 @Autowired
	 CityExcelService cityExcelService;
	 
	 @Autowired
	 DistrictExcelService districtExcelService;
	 
	 @Autowired
	 WardExcelService wardExcelService;
	 
	 @Autowired
	 UnitExcelService unitExcelService;
	 
	 @PostMapping("/upload/cities")
	  public ResponseEntity<ResponseMessage> uploadFileCity(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    if (CityExcelHelper.hasExcelFormat(file)) {
	      try {
	    	  cityExcelService.save(file);
	        message = "Uploaded the file successfully: " + file.getOriginalFilename();
	        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	      } catch (Exception e) {
	    	  System.out.println(e.getMessage());
	        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	      }
	    }
	    message = "Please upload an excel file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	  }
	 
	 @PostMapping("/upload/districts")
	 public ResponseEntity<ResponseMessage> uploadFileDistrict(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    if (DistrictExcelHelper.hasExcelFormat(file)) {
	      try {
	    	districtExcelService.save(file);
	        message = "Uploaded the file successfully: " + file.getOriginalFilename();
	        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	      } catch (Exception e) {
	    	System.out.println(e.getMessage());
	        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	      }
	    }
	    message = "Please upload an excel file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	 }
	 
	 @PostMapping("/upload/wards")
	 public ResponseEntity<ResponseMessage> uploadFileWard(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    if (WardExcelHelper.hasExcelFormat(file)) {
	      try {
	    	wardExcelService.save(file);
	        message = "Uploaded the file successfully: " + file.getOriginalFilename();
	        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	      } catch (Exception e) {
	    	System.out.println(e.getMessage());
	        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	      }
	    }
	    message = "Please upload an excel file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	 }
	 
	 @PostMapping("/upload/units")
	 public ResponseEntity<ResponseMessage> uploadFileUnit(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    if (UnitExcelHelper.hasExcelFormat(file)) {
	      try {
	    	  unitExcelService.save(file);
	        message = "Uploaded the file successfully: " + file.getOriginalFilename();
	        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	      } catch (Exception e) {
	    	  System.out.println(e);
	        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	      }
	    }
	    message = "Please upload an excel file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	 }
	
}
