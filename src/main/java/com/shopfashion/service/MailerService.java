package com.shopfashion.service;

import javax.mail.MessagingException;

import com.shopfashion.entity.Mail;


public interface MailerService {

	void send(Mail mail) throws MessagingException;
	
	void send(String to, String subject, String body) throws MessagingException;
	
}
