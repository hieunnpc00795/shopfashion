package com.shopfashion.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

public interface SessionService {

	ResponseEntity<?> getSession(HttpServletRequest request);

}
