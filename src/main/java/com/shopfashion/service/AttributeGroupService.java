package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.AttributeGroup;
import com.shopfashion.validation.AttributeGroupCreateValidation;
import com.shopfashion.validation.AttributeGroupUpdateValidation;

public interface AttributeGroupService {

	List<AttributeGroup> findAllAttributeGroup();

	ResponseEntity<?> createAttributeGroup(AttributeGroupCreateValidation attributeGroupCreateValidation);
	
	ResponseEntity<?> updateAttributeGroup(AttributeGroupUpdateValidation attributeGroupUpdateValidation);

	ResponseEntity<?> deleteAttributeGroup(@Valid AttributeGroupUpdateValidation attributeGroupUpdateValidation);

	List<AttributeGroup> search(String key_search);

}
