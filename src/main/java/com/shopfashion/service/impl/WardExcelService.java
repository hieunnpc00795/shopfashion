package com.shopfashion.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.common.DistrictExcelHelper;
import com.shopfashion.common.WardExcelHelper;
import com.shopfashion.dao.CityDAO;
import com.shopfashion.dao.DistrictDAO;
import com.shopfashion.dao.WardDAO;
import com.shopfashion.dto.DistrictRequest;
import com.shopfashion.dto.WardRequest;
import com.shopfashion.entity.City;
import com.shopfashion.entity.District;
import com.shopfashion.entity.Ward;

@Service
public class WardExcelService {
	
	@Autowired
	WardDAO wardDAO;
	
	@Autowired
	DistrictDAO districtDAO;
	
	public void save(MultipartFile file) {
	    try {
	      List<WardRequest> wardRequests = WardExcelHelper.excelWards(file.getInputStream());
	      List<Ward> wards = new ArrayList<Ward>();
	      wardRequests.forEach(wd -> {
	    	  Ward ward = new Ward();
	    	  ward.setId(wd.getId());
	    	  ward.setCode(wd.getCode());
	    	  ward.setName(wd.getName());
	    	  ward.setType(wd.getType());
	    	  District district = districtDAO.findByCode(wd.getDistrict_code());
	    	  ward.setDistrict(district);
	    	  wards.add(ward);
	      });
	      wardDAO.saveAll(wards);
	    } catch (IOException e) {
	      throw new RuntimeException("fail to store excel data: " + e.getMessage());
	    }
	}
}
