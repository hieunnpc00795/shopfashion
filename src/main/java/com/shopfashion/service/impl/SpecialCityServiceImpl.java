package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.SpecialCityDAO;
import com.shopfashion.entity.SpecialCity;
import com.shopfashion.service.SpecialCityService;

@Service
public class SpecialCityServiceImpl implements SpecialCityService{

	@Autowired
	SpecialCityDAO specialCityDAO;

	@Override
	public List<SpecialCity> findByPrice(Integer priceID) {
		// TODO Auto-generated method stub
		return specialCityDAO.findByPrice(priceID);
	}

	@Override
	public SpecialCity create(SpecialCity special_city) {
		// TODO Auto-generated method stub
		return specialCityDAO.save(special_city);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		specialCityDAO.deleteById(id);
	}
	
}
