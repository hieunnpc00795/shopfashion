package com.shopfashion.service.impl;

import java.io.IOException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.common.UnitExcelHelper;
import com.shopfashion.dao.UnitDAO;
import com.shopfashion.entity.Unit;

@Service
public class UnitExcelService {
	
	@Autowired
	UnitDAO unitDAO;
	
	public void save(MultipartFile file) {
	    try {
	      List<Unit> tutorials = UnitExcelHelper.excelUnits(file.getInputStream());
	      unitDAO.saveAll(tutorials);
	    } catch (IOException e) {
	      throw new RuntimeException("fail to store excel data: " + e.getMessage());
	    }
	}
	
	public List<Unit> getAllTutorials() {
	    return unitDAO.findAll();
	}
	
}
