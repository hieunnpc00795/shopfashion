package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.AttributeDAO;
import com.shopfashion.entity.Attribute;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.AttributeService;
import com.shopfashion.validation.AttributeCreateValidation;
import com.shopfashion.validation.AttributeUpdateValidation;

@Service
public class AttributeServiceImpl implements AttributeService{

	@Autowired
	AttributeDAO attributeDAO;

	@Override
	public List<Attribute> findAllAttributes() {
		// TODO Auto-generated method stub
		return attributeDAO.findAllAttributes();
	}
	
	@Override
	public List<Attribute> findAttributesByAttributeGroup(Integer group_id) {
		// TODO Auto-generated method stub
		return attributeDAO.findAttributesByAttributeGroup(group_id);
	}

	@Override
	public ResponseEntity<?> createAttribute(AttributeCreateValidation attributeCreateValidation) {
		// TODO Auto-generated method stub
		if(!attributeCreateValidation.getAttribute_group().getType().equals("TYPE_IMAGE")) {
			if(attributeCreateValidation.getValue() == null || attributeCreateValidation.getValue().isEmpty()) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng nhập giá trị cho thuộc tính.", "value"));
			}
		}
		Attribute attribute = new Attribute(
				attributeCreateValidation.getName(),
				attributeCreateValidation.getDescription(),
				attributeCreateValidation.getValue(),
				attributeCreateValidation.getActivity(),
				false,
				attributeCreateValidation.getAttribute_group());
		attributeDAO.save(attribute);
		return ResponseEntity.ok(new ResponseMessage("Thuộc tính được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> updateAttribute(AttributeUpdateValidation attributeUpdateValidation) {
		// TODO Auto-generated method stub
		Attribute attribute = attributeDAO.findById(attributeUpdateValidation.getId()).get();
		if(attribute == null || attribute.getDeleted() == true || attribute.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Thuộc tính không tồn tại.", "attribute"));
		}
		if(!attributeUpdateValidation.getAttribute_group().getType().equals("TYPE_IMAGE")) {
			if(attributeUpdateValidation.getValue() == null || attributeUpdateValidation.getValue().isEmpty()) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng nhập giá trị cho thuộc tính.", "value"));
			}
		}
		attribute.setName(attributeUpdateValidation.getName());
		attribute.setDescription(attributeUpdateValidation.getDescription());
		attribute.setValue(attributeUpdateValidation.getValue());
		attribute.setActivity(attributeUpdateValidation.getActivity());
		attribute.setAttribute_group(attributeUpdateValidation.getAttribute_group());
		attributeDAO.save(attribute);
		return ResponseEntity.ok(new ResponseMessage("Thuộc tính được cập nhật thành công!"));
	}

	@Override
	public ResponseEntity<?> deleteAttribute(AttributeUpdateValidation attributeUpdateValidation) {
		// TODO Auto-generated method stub
		Attribute attribute = attributeDAO.findById(attributeUpdateValidation.getId()).get();
		if(attribute == null || attribute.getDeleted() == true || attribute.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Thuộc tính không tồn tại.", "attribute"));
		}
		attribute.setDeleted(true);
	//	attribute.setDeleted_by(null);
		attributeDAO.save(attribute);
		return ResponseEntity.ok(new ResponseMessage("Thuộc tính được xóa thành công!"));
	}

	@Override
	public List<Attribute> search(String key_search) {
		// TODO Auto-generated method stub
		if(key_search.equals("null")) {
			return attributeDAO.findAllAttributes();
		}
		return attributeDAO.search(key_search);
	}
	
}
