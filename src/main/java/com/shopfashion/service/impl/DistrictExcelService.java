package com.shopfashion.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.common.DistrictExcelHelper;
import com.shopfashion.dao.CityDAO;
import com.shopfashion.dao.DistrictDAO;
import com.shopfashion.dto.DistrictRequest;
import com.shopfashion.entity.City;
import com.shopfashion.entity.District;

@Service
public class DistrictExcelService {
	
	@Autowired
	DistrictDAO districtDAO;
	
	@Autowired
	CityDAO cityDAO;
	
	public void save(MultipartFile file) {
	    try {
	      List<DistrictRequest> districtRequests = DistrictExcelHelper.excelDistricts(file.getInputStream());
	      List<District> districts = new ArrayList<District>();
	      districtRequests.forEach(dt -> {
	    	  District district = new District();
	    	  district.setId(dt.getId());
	    	  district.setCode(dt.getCode());
	    	  district.setName(dt.getName());
	    	  district.setType(dt.getType());
	    	  City city = cityDAO.findByCode(dt.getCity_code());
	    	  district.setCity(city);
	    	  districts.add(district);
	      });
	      districtDAO.saveAll(districts);
	    } catch (IOException e) {
	      throw new RuntimeException("fail to store excel data: " + e.getMessage());
	    }
	}
}
