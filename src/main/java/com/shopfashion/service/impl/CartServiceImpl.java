package com.shopfashion.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.common.CartUtils;
import com.shopfashion.common.JwtUtils;
import com.shopfashion.dao.AccountDAO;
import com.shopfashion.dao.CartDAO;
import com.shopfashion.dao.CartDetailDAO;
import com.shopfashion.dao.SessionDAO;
import com.shopfashion.dto.CartRequest;
import com.shopfashion.dto.CartResponse;
import com.shopfashion.entity.Account;
import com.shopfashion.entity.Cart;
import com.shopfashion.entity.CartDetail;
import com.shopfashion.entity.Session;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.CartService;
import com.shopfashion.validation.AddToCartValidation;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	CartDAO cartDAO;
	
	@Autowired
	CartDetailDAO cartDetailDAO;
	
	@Autowired
	CartUtils cartUtils;
	
	@Autowired
	SessionDAO sessionDAO;
	
	@Autowired
	AccountDAO accountDAO;
	
	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	HttpServletRequest request;
	
	@Override
	public ResponseEntity<?> detail(String session_id, String token) {
		// TODO Auto-generated method stub
		if(!session_id.equals("null") && !token.equals("null")) {
			String user_current = request.getRemoteUser();
			Account account = null;
			String username = jwtUtils.getUserNameFromJwtToken(token);
			if(user_current != null && user_current.equals(username)) {
				account = accountDAO.findAccountByUsername(username);
				if(account != null) {
					Cart cart = cartDAO.findByAccount(account.getId());
					if(cart != null) {
						List<CartDetail> cart_details = cartDetailDAO.findByCart(cart.getId());
						return ResponseEntity.ok(new CartResponse(cart, cart_details));
					} else {
						return ResponseEntity.ok(null);
					}
				} else {
					//session
					Session session = sessionDAO.findBySessionID(session_id);
					if(session != null) {
						Cart cart = cartDAO.findBySession(session.getId());
						if(cart != null) {
							List<CartDetail> cart_details = cartDetailDAO.findByCart(cart.getId());
							return ResponseEntity.ok(new CartResponse(cart, cart_details));
						} else {
							return ResponseEntity.ok(null);
						}
					} else {
						return ResponseEntity.ok(null);
					}
				}
			} else {
				Session session = sessionDAO.findBySessionID(session_id);
				if(session != null) {
					Cart cart = cartDAO.findBySession(session.getId());
					if(cart != null) {
						List<CartDetail> cart_details = cartDetailDAO.findByCart(cart.getId());
						return ResponseEntity.ok(new CartResponse(cart, cart_details));
					} else {
						return ResponseEntity.ok(null);
					}
				} else {
					return ResponseEntity.ok(null);
				}	
			}
		}
		Session session = sessionDAO.findBySessionID(session_id);
		if(session != null) {
			Cart cart = cartDAO.findBySession(session.getId());
			if(cart != null) {
				List<CartDetail> cart_details = cartDetailDAO.findByCart(cart.getId());
				return ResponseEntity.ok(new CartResponse(cart, cart_details));
			} else {
				return ResponseEntity.ok(null);
			}
		} else {
			return ResponseEntity.ok(null);
		}	
	}

	@Override
	public ResponseEntity<?> totalProductsInCart(CartRequest cartRequest) {
		// TODO Auto-generated method stub
		String user_current = request.getRemoteUser();
		Account account = null;
		if(cartRequest.getToken() != null && user_current != null) {
			String username = jwtUtils.getUserNameFromJwtToken(cartRequest.getToken());
			if(user_current.equals(username)) {
				account = accountDAO.findAccountByUsername(username);
			} else {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Token không tồn tại!", "token"));
			}
		}
		Session session = sessionDAO.findBySessionID(cartRequest.getSession_id());
		if(session.getId() == null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Session không tồn tại!", "session"));
		}
		if(account != null && session != null) {
			Cart user_cart = cartDAO.findByAccount(account.getId());
			if(user_cart != null) {
				Integer total = cartDetailDAO.countProductsByCart(user_cart.getId());
				return ResponseEntity.ok(total);
			} else {
				return ResponseEntity.ok(0);
			}
		}
		Cart client_cart = cartDAO.findBySession(session.getId());
		if(client_cart != null) {
			Integer total = cartDetailDAO.countProductsByCart(client_cart.getId());
			return ResponseEntity.ok(total);
		}
		return ResponseEntity.ok(0);
	}
	
	@Override
	public ResponseEntity<?> addToCart(AddToCartValidation addToCartValidation) {
		// TODO Auto-generated method stub
		String user_current = request.getRemoteUser();
		Account account = null;
		if(addToCartValidation.getToken() != null && user_current != null) {
			String username = jwtUtils.getUserNameFromJwtToken(addToCartValidation.getToken());
			if(user_current.equals(username)) {
				account = accountDAO.findAccountByUsername(username);
			} else {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Token không tồn tại!", "token"));
			}
		}
		Session session = sessionDAO.findBySessionID(addToCartValidation.getSession_id());
		if(session.getId() == null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Session không tồn tại!", "session"));
		}
		if(account != null && session.getId() != null) {
			cartUtils.mergeClientCartToUserCart(account, session);
			return cartUtils.userAddToCart(addToCartValidation, account, session);
		}
		if(account == null && session.getId() != null) {
			return cartUtils.clientAddToCart(addToCartValidation, session);
		}
		return ResponseEntity.badRequest().body(new ResponseMessage("Lỗi thêm vào giỏ hàng!"));
	}

	@Override
	public void mergeClientCartToUserCart(CartRequest cartRequest) {
		// TODO Auto-generated method stub
		String user_current = request.getRemoteUser();
		Account account = null;
		if(cartRequest.getToken() != null && user_current != null) {
			String username = jwtUtils.getUserNameFromJwtToken(cartRequest.getToken());
			if(user_current.equals(username)) {
				account = accountDAO.findAccountByUsername(username);
			} 
		}
		Session session = sessionDAO.findBySessionID(cartRequest.getSession_id());
		if(account != null && session.getId() != null) {
			cartUtils.mergeClientCartToUserCart(account, session);
		}
	}

}
