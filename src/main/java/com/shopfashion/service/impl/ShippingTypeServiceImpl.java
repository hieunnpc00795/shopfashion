package com.shopfashion.service.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.ShippingTypeDAO;
import com.shopfashion.entity.ShippingType;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.ShippingTypeService;
import com.shopfashion.validation.ShippingTypeCreateValidation;
import com.shopfashion.validation.ShippingTypeUpdateValidation;

@Service
public class ShippingTypeServiceImpl implements ShippingTypeService{

	@Autowired
	ShippingTypeDAO shippingTypeDAO;

	@Override
	public List<ShippingType> getListShippingTypes() {
		// TODO Auto-generated method stub
		return shippingTypeDAO.getListShippingTypes();
	}

	@Override
	public ResponseEntity<?> create(@Valid ShippingTypeCreateValidation shippingTypeCreateValidation) {
		// TODO Auto-generated method stub
		if(shippingTypeCreateValidation.getLogo().equals("mailchimp.png")) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn icon cho nhóm danh mục.", "logo"));
		}
		if(shippingTypeDAO.existsByCode(shippingTypeCreateValidation.getCode())) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Mã nhóm danh mục đã được sử dụng.", "code"));
		}
		ShippingType shipping_type = new ShippingType(
				shippingTypeCreateValidation.getCode(),
				shippingTypeCreateValidation.getName(),
				shippingTypeCreateValidation.getLogo(),
				shippingTypeCreateValidation.getActivity(),
				shippingTypeCreateValidation.getDescription(), 
				false,
				null);
		shippingTypeDAO.save(shipping_type);
		return ResponseEntity.ok(new ResponseMessage("Đơn vị vận chuyển được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> update(@Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation) {
		// TODO Auto-generated method stub
		if(shippingTypeUpdateValidation.getLogo().equals("mailchimp.png")) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn icon cho nhóm danh mục.", "logo"));
		}
		ShippingType shipping_type = shippingTypeDAO.findById(shippingTypeUpdateValidation.getId()).get();
		if(shipping_type == null || shipping_type.getDeleted() == true || shipping_type.getDeleted_by() != null){
			return ResponseEntity.badRequest().body(new ResponseMessageError("Đơn vị không tồn tại.", "shipping_type"));
		}
		shipping_type.setName(shippingTypeUpdateValidation.getName());
		shipping_type.setLogo(shippingTypeUpdateValidation.getLogo());
		shipping_type.setActivity(shippingTypeUpdateValidation.getActivity());
		shipping_type.setDescription(shippingTypeUpdateValidation.getDescription());
		shippingTypeDAO.save(shipping_type);
		return ResponseEntity.ok(new ResponseMessage("Đơn vị được cập nhật thành công!"));
	}

	@Override
	public ResponseEntity<?> delete(@Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation) {
		// TODO Auto-generated method stub
		ShippingType shipping_type = shippingTypeDAO.findById(shippingTypeUpdateValidation.getId()).get();
		if(shipping_type == null || shipping_type.getDeleted() == true || shipping_type.getDeleted_by() != null){
			return ResponseEntity.badRequest().body(new ResponseMessageError("Đơn vị không tồn tại.", "shipping_type"));
		}
		shipping_type.setDeleted(true);
	//	shipping_type.setDeleted_by(null);
		shippingTypeDAO.save(shipping_type);
		return ResponseEntity.ok(new ResponseMessage("Đơn vị được xóa thành công!"));
	}
	
}
