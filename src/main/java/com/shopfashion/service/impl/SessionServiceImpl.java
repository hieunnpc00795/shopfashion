package com.shopfashion.service.impl;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.SessionDAO;
import com.shopfashion.dto.SessionResponse;
import com.shopfashion.entity.Session;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.SessionService;

@Service
public class SessionServiceImpl implements SessionService{

	@Autowired
	SessionDAO sessionDAO;

	@Override
	public ResponseEntity<?> getSession(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String ip = request.getHeader("X-Forwarded-For");  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("Proxy-Client-IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("WL-Proxy-Client-IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_FORWARDED");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_CLIENT_IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_FORWARDED_FOR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_FORWARDED");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_VIA");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("REMOTE_ADDR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getRemoteAddr();  
	    }
	    SessionResponse session_response = new SessionResponse();
	    try {
			Session result = sessionDAO.findByIP(ip);
			if(result != null) {
				session_response.setSession_id(result.getSession_id());
			} else {
				UUID general_string = UUID.randomUUID();
				Session session = new Session(
						general_string.toString(),
						ip,
						request.getHeader("User-Agent"));
				session_response.setSession_id(session.getSession_id());
				sessionDAO.save(session);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body(new ResponseMessage(e.getMessage()));		
		}
		return ResponseEntity.ok(session_response);
	}
	
}
