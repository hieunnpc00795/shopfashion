package com.shopfashion.service.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.AttributeGroupDAO;
import com.shopfashion.entity.AttributeGroup;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.AttributeGroupService;
import com.shopfashion.validation.AttributeGroupCreateValidation;
import com.shopfashion.validation.AttributeGroupUpdateValidation;

@Service
public class AttributeGroupServiceImpl implements AttributeGroupService{

	@Autowired
	AttributeGroupDAO attributeGroupDAO;

	@Override
	public List<AttributeGroup> findAllAttributeGroup() {
		// TODO Auto-generated method stub
		return attributeGroupDAO.findAllAttributeGroups();
	}

	@Override
	public ResponseEntity<?> createAttributeGroup(AttributeGroupCreateValidation attributeGroupCreateValidation) {
		// TODO Auto-generated method stub
		if(attributeGroupDAO.existsByCode(attributeGroupCreateValidation.getCode())) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Mã nhóm thuộc tính đã được sử dụng.", "code"));
		}
		AttributeGroup attributeGroup = new AttributeGroup(
				attributeGroupCreateValidation.getCode(),
				attributeGroupCreateValidation.getName(),
				attributeGroupCreateValidation.getType(),
				Integer.parseInt(attributeGroupCreateValidation.getLevel()),
				attributeGroupCreateValidation.getDescription(),
				attributeGroupCreateValidation.getActivity(),
				false);
		attributeGroupDAO.save(attributeGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm thuộc tính được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> updateAttributeGroup(AttributeGroupUpdateValidation attributeGroupUpdateValidation) {
		// TODO Auto-generated method stub
		AttributeGroup attributeGroup = attributeGroupDAO.findById(attributeGroupUpdateValidation.getId()).get();
		if(attributeGroup == null || attributeGroup.getDeleted() == true || attributeGroup.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Nhóm thuộc tính không tồn tại.", "attribute_group"));
		}
		attributeGroup.setName(attributeGroupUpdateValidation.getName());
		attributeGroup.setType(attributeGroupUpdateValidation.getType());
		attributeGroup.setLevel(Integer.parseInt(attributeGroupUpdateValidation.getLevel()));
		attributeGroup.setActivity(attributeGroupUpdateValidation.getActivity());
		attributeGroup.setDescription(attributeGroupUpdateValidation.getDescription());
		attributeGroupDAO.save(attributeGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm thuộc tính được cập nhật thành công!"));
	}

	@Override
	public ResponseEntity<?> deleteAttributeGroup(@Valid AttributeGroupUpdateValidation attributeGroupUpdateValidation) {
		// TODO Auto-generated method stub
		AttributeGroup attributeGroup = attributeGroupDAO.findById(attributeGroupUpdateValidation.getId()).get();
		if(attributeGroup == null || attributeGroup.getDeleted() == true || attributeGroup.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Nhóm thuộc tính không tồn tại.", "attribute_group"));
		}
		attributeGroup.setDeleted(true);
	//	attributeGroup.setDeleted_by(null);
		attributeGroupDAO.save(attributeGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm thuộc tính được xóa thành công!"));
	}

	@Override
	public List<AttributeGroup> search(String key_search) {
		// TODO Auto-generated method stub
		if(key_search.equals("null")) {
			return attributeGroupDAO.findAllAttributeGroups();
		}
		return attributeGroupDAO.search(key_search);
	}
	
}
