package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.CategoryDAO;
import com.shopfashion.entity.Category;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.CategoryService;
import com.shopfashion.validation.CategoryCreateValidation;
import com.shopfashion.validation.CategoryUpdateValidation;

@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	CategoryDAO categoryDAO;

	@Override
	public List<Category> findAllCategory() {
		// TODO Auto-generated method stub
		return categoryDAO.findAllCategory();
	}
	
	@Override
	public List<Category> findCategoriesByCategoryGroup(Integer group_id) {
		// TODO Auto-generated method stub
		return categoryDAO.findCategoriesByCategoryGroup(group_id);
	}

	@Override
	public ResponseEntity<?> createCategory(CategoryCreateValidation categoryCreateValidation) {
		// TODO Auto-generated method stub
		if(categoryDAO.existsByCode(categoryCreateValidation.getCode())) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Mã danh mục đã được sử dụng.", "code"));
		}
		Category category = new Category(
				categoryCreateValidation.getCode(),
				categoryCreateValidation.getName(),
				Integer.parseInt(categoryCreateValidation.getLevel()),
				categoryCreateValidation.getView(),
				categoryCreateValidation.getActivity(),
				false,
				categoryCreateValidation.getCategoryGroup());
		categoryDAO.save(category);
		return ResponseEntity.ok(new ResponseMessage("Danh mục được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> updateCategory(CategoryUpdateValidation categoryUpdateValidation) {
		// TODO Auto-generated method stub
		Category category = categoryDAO.findById(categoryUpdateValidation.getId()).get();
		if(category == null || category.getDeleted() == true || category.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Danh mục không tồn tại.", "category"));
		}
		category.setName(categoryUpdateValidation.getName());
		category.setLevel(Integer.parseInt(categoryUpdateValidation.getLevel()));
		category.setView(categoryUpdateValidation.getView());
		category.setCategoryGroup(categoryUpdateValidation.getCategoryGroup());
		category.setActivity(categoryUpdateValidation.getActivity());
		categoryDAO.save(category);
		return ResponseEntity.ok(new ResponseMessage("Danh mục được cập nhật thành công!"));
	}

	@Override
	public ResponseEntity<?> deleteCategory(CategoryUpdateValidation categoryUpdateValidation) {
		// TODO Auto-generated method stub
		Category category = categoryDAO.findById(categoryUpdateValidation.getId()).get();
		if(category == null || category.getDeleted() == true || category.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Danh mục không tồn tại.", "category"));
		}
		category.setDeleted(true);
//		category.setDeleted_by(null);
		categoryDAO.save(category);
		return ResponseEntity.ok(new ResponseMessage("Danh mục được xóa thành công!"));
	}

	@Override
	public List<Category> search(String key_search) {
		// TODO Auto-generated method stub
		if(key_search.equals("null")) {
			return categoryDAO.findAllCategory();
		}
		return categoryDAO.search(key_search);
	}
	
}
