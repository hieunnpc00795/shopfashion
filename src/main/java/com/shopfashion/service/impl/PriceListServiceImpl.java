package com.shopfashion.service.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.PriceListDAO;
import com.shopfashion.entity.PriceList;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.PriceListService;
import com.shopfashion.validation.PriceListCreateValidation;
import com.shopfashion.validation.PriceListUpdateValidation;

@Service
public class PriceListServiceImpl implements PriceListService{

	@Autowired
	PriceListDAO priceListDAO;

	@Override
	public List<PriceList> findAll() {
		// TODO Auto-generated method stub
		return priceListDAO.findPriceLists();
	}

	@Override
	public ResponseEntity<?> create(@Valid PriceListCreateValidation priceListCreateValidation) {
		// TODO Auto-generated method stub
		PriceList price_list = priceListDAO.findByShippingType(priceListCreateValidation.getShipping_type().getId());
		if(price_list != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Đơn vị vận chuyển đã có bảng giá!", "price_list"));
		}
		price_list = new PriceList(
				priceListCreateValidation.getName(),
				Integer.parseInt(priceListCreateValidation.getCod_fee()),
				Integer.parseInt(priceListCreateValidation.getCommodity_value()),
				Float.parseFloat(priceListCreateValidation.getBigger()),
				priceListCreateValidation.getType_bigger(),
				Float.parseFloat(priceListCreateValidation.getLess_than()),
				priceListCreateValidation.getType_less_than(),
				priceListCreateValidation.getActivity(),
				false,
				null,
				priceListCreateValidation.getShipping_type(),
				priceListCreateValidation.getCities());
		priceListDAO.save(price_list);
		return ResponseEntity.ok(new ResponseMessage("Bảng giá được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> update(@Valid PriceListUpdateValidation priceListUpdateValidation) {
		// TODO Auto-generated method stub
		PriceList price_list = priceListDAO.findById(priceListUpdateValidation.getId()).get();
		if(price_list == null || price_list.getDeleted() == true || price_list.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Bảng giá không tồn tại!", "price_list"));
		}
		price_list.setName(priceListUpdateValidation.getName());
		price_list.setCod_fee(Integer.parseInt(priceListUpdateValidation.getCod_fee()));
		price_list.setCommodity_value(Integer.parseInt(priceListUpdateValidation.getCommodity_value()));
		price_list.setBigger(Float.parseFloat(priceListUpdateValidation.getBigger()));
		price_list.setType_bigger(priceListUpdateValidation.getType_bigger());
		price_list.setLess_than(Float.parseFloat(priceListUpdateValidation.getLess_than()));
		price_list.setType_less_than(priceListUpdateValidation.getType_less_than());
		price_list.setActivity(priceListUpdateValidation.getActivity());
		price_list.setCities(priceListUpdateValidation.getCities());
		priceListDAO.save(price_list);
		return ResponseEntity.ok(new ResponseMessage("Bảng giá được cập nhật thành công!"));
	}
	
}
