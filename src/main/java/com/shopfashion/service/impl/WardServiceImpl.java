package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.WardDAO;
import com.shopfashion.entity.Ward;
import com.shopfashion.service.WardService;

@Service
public class WardServiceImpl implements WardService{

	@Autowired
	WardDAO wardDAO;

	@Override
	public List<Ward> getListWardsByDistrict(Integer districtId) {
		// TODO Auto-generated method stub
		return wardDAO.getListWardsByDistrict(districtId);
	}
	
}
