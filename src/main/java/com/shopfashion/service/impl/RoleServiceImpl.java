package com.shopfashion.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.RoleDAO;
import com.shopfashion.entity.Role;
import com.shopfashion.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	RoleDAO roleDAO;

	@Override
	public List<Role> getListRole() {
		// TODO Auto-generated method stub
		return roleDAO.findAll();
	}
	
}
