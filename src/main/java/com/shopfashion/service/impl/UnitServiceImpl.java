package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.UnitDAO;
import com.shopfashion.entity.Unit;
import com.shopfashion.service.UnitService;

@Service
public class UnitServiceImpl implements UnitService{

	@Autowired
	UnitDAO unitDAO;

	@Override
	public List<Unit> findAllUnits() {
		// TODO Auto-generated method stub
		return unitDAO.findAll();
	}
	
}
