package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.AuthorityDAO;
import com.shopfashion.entity.Authority;
import com.shopfashion.service.AuthorityService;

@Service
public class AuthorityServiceImpl implements AuthorityService{

	@Autowired
	AuthorityDAO authorityDAO;

	@Override
	public List<Authority> findAll() {
		// TODO Auto-generated method stub
		return authorityDAO.findAll();
	}

	@Override
	public Authority create(Authority authority) {
		// TODO Auto-generated method stub
		return authorityDAO.save(authority);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		authorityDAO.deleteById(id);
	}
	
}
