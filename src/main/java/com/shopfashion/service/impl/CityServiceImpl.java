package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.CityDAO;
import com.shopfashion.entity.City;
import com.shopfashion.service.CityService;

@Service
public class CityServiceImpl implements CityService{
	
	@Autowired
	CityDAO cityDAO;

	@Override
	public List<City> getListCities() {
		// TODO Auto-generated method stub
		return cityDAO.findAll();
	}

}
