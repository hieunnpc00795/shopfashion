package com.shopfashion.service.impl;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.ProductAttributeDAO;
import com.shopfashion.dao.ProductVariantDAO;
import com.shopfashion.dto.AttributeRequest;
import com.shopfashion.dto.AttributeResponse;
import com.shopfashion.entity.Attribute;
import com.shopfashion.entity.ProductAttribute;
import com.shopfashion.entity.ProductVariant;
import com.shopfashion.service.ProductAttributeService;
import com.shopfashion.validation.GetVariantByAttributesValidation;

@Service
public class ProductAttributeServiceImpl implements ProductAttributeService{

	@Autowired
	ProductAttributeDAO productAttributeDAO;
	
	@Autowired
	ProductVariantDAO productVariantDAO;

	@Override
	public AttributeResponse getListAttribute(Integer attID, Integer level, 
			GetVariantByAttributesValidation getVariantByAttributesValidation) {
		// TODO Auto-generated method stub
		List<ProductVariant> product_variants = productVariantDAO.findVariantsByProduct(getVariantByAttributesValidation.getProduct_id());
		List<ProductVariant> product_variants_filter = new ArrayList<>();
		for(ProductVariant product_variant:product_variants) {
			List<ProductAttribute> product_attributes = productAttributeDAO.findByVariantID(product_variant.getId());
			int result = getVariantByAttributesValidation.getAttributes().size();
			int check_result = 0;
			for(ProductAttribute product_attribute:product_attributes) {
				for(AttributeRequest attribute_request:getVariantByAttributesValidation.getAttributes()) {
					if(product_attribute.getAttribute().getId() == attribute_request.getAttribute_id() 
							&& product_attribute.getLevel() == attribute_request.getId()) {
						check_result++;
					}
				}
			}
			if(result == check_result) {
				product_variants_filter.add(product_variant);
			}
		}
		List<Attribute> attributes = new ArrayList<>();
		level++;
		if(product_variants_filter.size() > 0) {
			for(ProductVariant product_variant:product_variants_filter) {
				Attribute attribute_filter = productAttributeDAO.findByVariantIDAndLevel(product_variant.getId(), level);
				if(attribute_filter != null) {
					if(attributes.size() == 0) {
						attributes.add(attribute_filter);
					} 
					if(attributes.size() > 0) {
						boolean check_attribute = false;
						for(Attribute attribute:attributes) {
							if(attribute.getId() == attribute_filter.getId()) {
								check_attribute = true;
							}
						}
						if(check_attribute == false) {
							attributes.add(attribute_filter);
						}
					}
				}
			}
			if(attributes.size() > 0) {
				for(ProductVariant product_variant:product_variants_filter) {
					if(product_variant.getAttributes().size() < level) {
						Attribute attribute = new Attribute(0, "THƯỜNG", "THƯỜNG");
						attributes.add(attribute);
						break;
					}
				}
			}
		}
		if(attributes.size() > 0) {
			AttributeResponse attribute_response = new AttributeResponse(level, attributes);
			return attribute_response;
		}
		return null;
	}
	
}
