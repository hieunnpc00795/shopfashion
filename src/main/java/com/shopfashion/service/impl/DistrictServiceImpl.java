package com.shopfashion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.DistrictDAO;
import com.shopfashion.entity.District;
import com.shopfashion.service.DistrictService;

@Service
public class DistrictServiceImpl implements DistrictService{

	@Autowired
	DistrictDAO districtDAO;

	@Override
	public List<District> getListDistrictsByCity(Integer cityId) {
		// TODO Auto-generated method stub
		return districtDAO.getListDistrictsByCity(cityId);
	}
	
}
