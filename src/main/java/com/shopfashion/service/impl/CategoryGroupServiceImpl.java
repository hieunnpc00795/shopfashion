package com.shopfashion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.shopfashion.dao.CategoryGroupDAO;
import com.shopfashion.entity.CategoryGroup;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.service.CategoryGroupService;
import com.shopfashion.validation.CategoryGroupCreateValidation;
import com.shopfashion.validation.CategoryGroupUpdateValidation;

@Service
public class CategoryGroupServiceImpl implements CategoryGroupService{

	@Autowired
	CategoryGroupDAO categoryGroupDAO;
	
	@Override
	public List<CategoryGroup> findAllCategoryGroup() {
		// TODO Auto-generated method stub
		return categoryGroupDAO.findAllCategoryGroup(Sort.by(Direction.ASC, "level"));
	}

	@Override
	public ResponseEntity<?> create(CategoryGroupCreateValidation categoryGroupCreateValidation) {
		// TODO Auto-generated method stub
		if(categoryGroupCreateValidation.getIcon().equals("mailchimp.png")) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn icon cho nhóm danh mục.", "icon"));
		}
		if(categoryGroupDAO.existsByCode(categoryGroupCreateValidation.getCode())) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Mã nhóm danh mục đã được sử dụng.", "code"));
		}
		CategoryGroup categoryGroup = new CategoryGroup(
				categoryGroupCreateValidation.getCode(),
				categoryGroupCreateValidation.getName(),
				Integer.parseInt(categoryGroupCreateValidation.getLevel()),
				categoryGroupCreateValidation.getIcon(),
				categoryGroupCreateValidation.getActivity(),
				false);
		categoryGroupDAO.save(categoryGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm danh mục được tạo thành công!"));
	}

	@Override
	public ResponseEntity<?> update(CategoryGroupUpdateValidation categoryGroupUpdateValidation) {
		// TODO Auto-generated method stub
		if(categoryGroupUpdateValidation.getIcon().equals("mailchimp.png")) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn icon cho nhóm danh mục.", "icon"));
		}
		CategoryGroup categoryGroup = categoryGroupDAO.findById(categoryGroupUpdateValidation.getId()).get();
		if(categoryGroup == null || categoryGroup.getDeleted() == true || categoryGroup.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Nhóm danh mục không tồn tại.", "category_group"));
		}
		categoryGroup.setName(categoryGroupUpdateValidation.getName());
		categoryGroup.setLevel(Integer.parseInt(categoryGroupUpdateValidation.getLevel()));
		categoryGroup.setIcon(categoryGroupUpdateValidation.getIcon());
		categoryGroup.setActivity(categoryGroupUpdateValidation.getActivity());
		categoryGroupDAO.save(categoryGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm danh mục được cập nhật thành công!"));
	}

	@Override
	public ResponseEntity<?> delete(CategoryGroupUpdateValidation categoryGroupUpdateValidation) {
		// TODO Auto-generated method stub
		CategoryGroup categoryGroup = categoryGroupDAO.findById(categoryGroupUpdateValidation.getId()).get();
		if(categoryGroup == null || categoryGroup.getDeleted() == true || categoryGroup.getDeleted_by() != null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Nhóm danh mục không tồn tại.", "category_group"));
		}
		categoryGroup.setDeleted(true);
//		categoryGroup.setDeleted_by(null);
		categoryGroupDAO.save(categoryGroup);
		return ResponseEntity.ok(new ResponseMessage("Nhóm danh mục được xóa thành công!"));
	}
	
}
