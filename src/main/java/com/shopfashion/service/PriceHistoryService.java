package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.PriceHistory;
import com.shopfashion.validation.PriceHistoryProductCreateValidation;
import com.shopfashion.validation.PriceHistoryVariantCreateValidation;

public interface PriceHistoryService {

	List<PriceHistory> getListPriceHistories();

	ResponseEntity<?> createPriceHistoryProduct(PriceHistoryProductCreateValidation priceHistoryProductCreateValidation);
	
	ResponseEntity<?> createPriceHistoryVariant(PriceHistoryVariantCreateValidation priceHistoryVariantCreateValidation);

	List<PriceHistory> search(String search);

}
