package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.Role;


public interface RoleService {

	List<Role> getListRole();

}
