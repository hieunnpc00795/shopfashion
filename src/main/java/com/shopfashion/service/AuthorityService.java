package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.Authority;

public interface AuthorityService {

	List<Authority> findAll();

	Authority create(Authority authority);

	void delete(Integer id);

}
