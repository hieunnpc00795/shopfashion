package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.Attribute;
import com.shopfashion.validation.AttributeCreateValidation;
import com.shopfashion.validation.AttributeUpdateValidation;

public interface AttributeService {

	List<Attribute> findAllAttributes();
	
	List<Attribute> findAttributesByAttributeGroup(Integer group_id);

	ResponseEntity<?> createAttribute(AttributeCreateValidation attributeCreateValidation);
	
	ResponseEntity<?> updateAttribute(AttributeUpdateValidation attributeUpdateValidation);
	
	ResponseEntity<?> deleteAttribute(AttributeUpdateValidation attributeUpdateValidation);

	List<Attribute> search(String key_search);

}
