package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.District;

public interface DistrictService {

	List<District> getListDistrictsByCity(Integer cityId);

}
