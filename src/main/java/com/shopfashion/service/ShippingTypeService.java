package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.ShippingType;
import com.shopfashion.validation.ShippingTypeCreateValidation;
import com.shopfashion.validation.ShippingTypeUpdateValidation;

public interface ShippingTypeService {

	List<ShippingType> getListShippingTypes();

	ResponseEntity<?> create(@Valid ShippingTypeCreateValidation shippingTypeCreateValidation);

	ResponseEntity<?> update(@Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation);

	ResponseEntity<?> delete(@Valid ShippingTypeUpdateValidation shippingTypeUpdateValidation);

}
