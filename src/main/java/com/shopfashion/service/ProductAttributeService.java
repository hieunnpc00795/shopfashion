package com.shopfashion.service;

import com.shopfashion.dto.AttributeRequest;
import com.shopfashion.dto.AttributeResponse;
import com.shopfashion.validation.GetVariantByAttributesValidation;

public interface ProductAttributeService {

	AttributeResponse getListAttribute(Integer attID, Integer level, GetVariantByAttributesValidation getVariantByAttributesValidation);

}
