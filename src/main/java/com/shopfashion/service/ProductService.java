package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.PriceHistory;
import com.shopfashion.entity.Product;
import com.shopfashion.validation.ProductCreateValidation;
import com.shopfashion.validation.ProductUpdateValidation;

public interface ProductService {

	List<Product> findAllProducts();

	ResponseEntity<?> createProduct(ProductCreateValidation productCreateValidation);
	
	ResponseEntity<?> updateProduct(ProductUpdateValidation productUpdateValidation);

	ResponseEntity<?> deleteProduct(ProductUpdateValidation productUpdateValidation);

	List<Product> search(String search);
	
	List<Product> findNewProducts();

	Product productDetail(Integer proId, String slug);
	
}
