package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.ProductVariant;
import com.shopfashion.validation.GetVariantByAttributesValidation;
import com.shopfashion.validation.ProductVariantCreateValidation;
import com.shopfashion.validation.ProductVariantUpdateValidation;

public interface ProductVariantService {

	List<ProductVariant> findVariantsByProduct(Integer product_id);

	ResponseEntity<?> createVariant(ProductVariantCreateValidation productVariantCreateValidation);
	
	ResponseEntity<?> updateVariant(ProductVariantUpdateValidation productVariantUpdateValidation);

	ResponseEntity<?> deleteVariant(ProductVariantUpdateValidation productVariantUpdateValidation);

	ResponseEntity<?> getVariantsByAttributes(GetVariantByAttributesValidation getVariantByAttributesValidation);
}
