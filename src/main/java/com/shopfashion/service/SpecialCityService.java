package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.SpecialCity;

public interface SpecialCityService {

	List<SpecialCity> findByPrice(Integer priceID);

	SpecialCity create(SpecialCity special_city);

	void delete(Integer id);

}
