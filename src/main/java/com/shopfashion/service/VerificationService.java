package com.shopfashion.service;

import org.springframework.http.ResponseEntity;

import com.shopfashion.validation.VerificationValidation;

public interface VerificationService {

	ResponseEntity<?> sendVerificationCodeForgotPassword(VerificationValidation forgotPasswordValidation);

}
