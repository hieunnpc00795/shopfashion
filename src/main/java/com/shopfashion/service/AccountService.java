package com.shopfashion.service;

import java.util.List;


import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import com.shopfashion.entity.Account;
import com.shopfashion.validation.AccountBlockValidation;
import com.shopfashion.validation.AccountCreateValidation;
import com.shopfashion.validation.AccountDeleteValidation;
import com.shopfashion.validation.AccountOpenValidation;
import com.shopfashion.validation.AccountUpdateValidation;
import com.shopfashion.validation.ForgotPasswordValidation;
import com.shopfashion.validation.RegisterValidation;
import com.shopfashion.validation.SigninValidation;

public interface AccountService {

	ResponseEntity<?> create(@Valid AccountCreateValidation accountCreateValidation);

	List<Account> findAll();

	ResponseEntity<?> update(AccountUpdateValidation accountUpdateValidation);

	ResponseEntity<?> delete(AccountDeleteValidation accountDeleteValidation);

	ResponseEntity<?> block(AccountBlockValidation accountBlockValidation);

	ResponseEntity<?> open(AccountOpenValidation accountOpenValidation);

	List<Account> search(String search);
	
	ResponseEntity<?> signin(SigninValidation signinValidation);

	ResponseEntity<?> register(RegisterValidation registerValidation);

	ResponseEntity<?> forgotPassword(ForgotPasswordValidation forgotPasswordValidation);

}
