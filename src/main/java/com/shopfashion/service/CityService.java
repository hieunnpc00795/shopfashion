package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.City;

public interface CityService {

	List<City> getListCities();

}
