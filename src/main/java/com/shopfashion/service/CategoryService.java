package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.Category;
import com.shopfashion.validation.CategoryCreateValidation;
import com.shopfashion.validation.CategoryUpdateValidation;

public interface CategoryService {

	List<Category> findAllCategory();
	
	List<Category> findCategoriesByCategoryGroup(Integer group_id);

	ResponseEntity<?> createCategory(CategoryCreateValidation categoryCreateValidation);

	ResponseEntity<?> updateCategory(CategoryUpdateValidation categoryUpdateValidation);
	
	ResponseEntity<?> deleteCategory(CategoryUpdateValidation categoryUpdateValidation);

	List<Category> search(String key_search);
}
