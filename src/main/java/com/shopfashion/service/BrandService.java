package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.Brand;
import com.shopfashion.validation.BrandCreateValidation;
import com.shopfashion.validation.BrandUpdateValidation;

public interface BrandService {

	List<Brand> findAll();

	ResponseEntity<?> createBrand(BrandCreateValidation brandCreateValidation);

	ResponseEntity<?> updateBrand(BrandUpdateValidation brandUpdateValidation);

	ResponseEntity<?> deleteBrand(BrandUpdateValidation brandUpdateValidation);

	List<Brand> search(String key_search);
}
