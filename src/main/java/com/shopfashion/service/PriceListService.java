package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.PriceList;
import com.shopfashion.validation.PriceListCreateValidation;
import com.shopfashion.validation.PriceListUpdateValidation;

public interface PriceListService {

	List<PriceList> findAll();

	ResponseEntity<?> create(@Valid PriceListCreateValidation priceListCreateValidation);

	ResponseEntity<?> update(@Valid PriceListUpdateValidation priceListUpdateValidation);

}
