package com.shopfashion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.entity.CategoryGroup;
import com.shopfashion.validation.CategoryGroupCreateValidation;
import com.shopfashion.validation.CategoryGroupUpdateValidation;

public interface CategoryGroupService {
	
	List<CategoryGroup> findAllCategoryGroup();

	ResponseEntity<?> create(CategoryGroupCreateValidation categoryGroupCreateValidation);

	ResponseEntity<?> update(CategoryGroupUpdateValidation categoryGroupUpdateValidation);

	ResponseEntity<?> delete(CategoryGroupUpdateValidation categoryGroupUpdateValidation);

}
