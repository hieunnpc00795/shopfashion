package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.Ward;

public interface WardService {

	List<Ward> getListWardsByDistrict(Integer districtId);

}
