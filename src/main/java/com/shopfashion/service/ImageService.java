package com.shopfashion.service;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.entity.ProductImage;

public interface ImageService {

	File save(MultipartFile file, String folder);

	void delete(String folder, String image);

}
