package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.ProductImage;

public interface ProductImageService {

	List<ProductImage> findImagesByProduct(Integer product_id);

	void deleteImage(Integer id);

}
