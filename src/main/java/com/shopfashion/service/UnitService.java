package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.Unit;

public interface UnitService {

	List<Unit> findAllUnits();

}
