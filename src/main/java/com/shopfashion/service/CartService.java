package com.shopfashion.service;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.shopfashion.dto.CartRequest;
import com.shopfashion.validation.AddToCartValidation;

public interface CartService {

	ResponseEntity<?> detail(String session_id, String token);
	
	ResponseEntity<?> totalProductsInCart(CartRequest cartRequest);
	
	ResponseEntity<?> addToCart(AddToCartValidation addToCartValidation);

	void mergeClientCartToUserCart(CartRequest cartRequest);

}
