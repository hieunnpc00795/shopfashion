package com.shopfashion.service;

import java.util.List;

import com.shopfashion.entity.ProductCategory;

public interface ProductCategoryService {

	List<ProductCategory> findAllProductCategory();

	ProductCategory create(ProductCategory productCategory);

	void delete(Integer id);

}
