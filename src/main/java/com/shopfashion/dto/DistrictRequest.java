package com.shopfashion.dto;

import lombok.Data;

@Data
public class DistrictRequest {

	Integer id;
	
	private String code;
	
	private String name;
	
	private String type;
	
	private String city_code;
	
	public DistrictRequest() {
		super();
	}

	public DistrictRequest(Integer id, String code, String name, String type, String city_code) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.city_code = city_code;
	}
	
}
