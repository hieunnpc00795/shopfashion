package com.shopfashion.dto;

import lombok.Data;

@Data
public class WardRequest {

	Integer id;
	
	private String code;
	
	private String name;
	
	private String type;
	
	private String district_code;
	
	public WardRequest() {
		super();
	}

	public WardRequest(Integer id, String code, String name, String type, String district_code) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.district_code = district_code;
	}
	
}
