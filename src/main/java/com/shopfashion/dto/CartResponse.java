package com.shopfashion.dto;

import java.util.List;

import com.shopfashion.entity.Cart;
import com.shopfashion.entity.CartDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartResponse {

	Cart cart;
	
	List<CartDetail> cart_details;
	
}
