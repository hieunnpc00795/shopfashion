package com.shopfashion.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
public class JwtResponse {

	private String token;
	private List<String> roles;
	
	public JwtResponse(String accessToken, List<String> roles) {
		this.token = accessToken;
		this.roles = roles;
	}

	public JwtResponse() {
		super();
	}
	
}
