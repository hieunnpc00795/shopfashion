package com.shopfashion.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartRequest {

	@NotNull(message = "Session_id không được để trống.")
	String session_id;
	
	String token;
	
	
}
