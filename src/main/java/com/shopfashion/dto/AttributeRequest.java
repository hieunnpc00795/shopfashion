package com.shopfashion.dto;

import lombok.Data;

@Data
public class AttributeRequest {

	Integer id;
	
	Integer attribute_id;
}
