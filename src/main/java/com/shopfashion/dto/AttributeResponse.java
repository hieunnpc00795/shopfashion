package com.shopfashion.dto;

import java.util.List;

import com.shopfashion.entity.Attribute;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AttributeResponse {

	Integer id;
	
	List<Attribute> attributes;
	
}
