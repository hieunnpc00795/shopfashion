package com.shopfashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopfashion.entity.Unit;

public interface UnitDAO extends JpaRepository<Unit, Integer>{

}
