package com.shopfashion.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopfashion.common.ERole;
import com.shopfashion.entity.Role;

public interface RoleDAO extends JpaRepository<Role, Integer>{
	
	Optional<Role> findByCode(ERole role);
	
}
