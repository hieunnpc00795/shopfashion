package com.shopfashion.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.Authority;

public interface AuthorityDAO extends JpaRepository<Authority, Integer>{
	
}
