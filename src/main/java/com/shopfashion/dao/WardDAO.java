package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.Ward;

public interface WardDAO extends JpaRepository<Ward, Integer>{

	@Query("select wd from Ward wd where wd.district.id=?1")
	List<Ward> getListWardsByDistrict(Integer districtId);

}
