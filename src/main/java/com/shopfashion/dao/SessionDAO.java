package com.shopfashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.Session;

public interface SessionDAO extends JpaRepository<Session, Integer>{

	@Query("SELECT session FROM Session session WHERE session.ip=?1")
	Session findByIP(String ip);
	
	@Query("SELECT session FROM Session session WHERE session.session_id=?1")
	Session findBySessionID(String session_id);
	
}
