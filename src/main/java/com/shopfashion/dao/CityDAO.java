package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.City;

public interface CityDAO extends JpaRepository<City, Integer>{

	@Query("select ct from City ct where ct.code=?1")
	City findByCode(String code);

}
