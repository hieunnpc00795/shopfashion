package com.shopfashion.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.shopfashion.entity.Account;

public interface AccountDAO extends JpaRepository<Account, Integer>{
	
	Optional<Account> findByUsername(String username);

	Boolean existsByUsername(String username);
	
	Boolean existsByPhone(String phone);
	
	Boolean existsByEmail(String email);

	@Query("SELECT ac FROM Account ac where ac.username LIKE %?1%")
	List<Account> search(String search);
	
	@Query("SELECT ac FROM Account ac where ac.username =?1")
	Account findAccountByUsername(String username);
}
