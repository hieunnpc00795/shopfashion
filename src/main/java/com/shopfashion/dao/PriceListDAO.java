package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.PriceList;

public interface PriceListDAO extends JpaRepository<PriceList, Integer>{

	@Query("SELECT price FROM PriceList price WHERE price.deleted=false AND price.deleted_by IS NULL")
	List<PriceList> findPriceLists();

	@Query("SELECT price FROM PriceList price WHERE price.shipping_type.id=?1 AND price.deleted_by IS NULL")
	PriceList findByShippingType(Integer id);

}
