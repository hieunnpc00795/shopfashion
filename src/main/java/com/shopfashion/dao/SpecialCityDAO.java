package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.SpecialCity;

public interface SpecialCityDAO extends JpaRepository<SpecialCity, Integer>{

	@Query("SELECT spec FROM SpecialCity spec WHERE spec.price_list.id=?1")
	List<SpecialCity> findByPrice(Integer priceID);

}
