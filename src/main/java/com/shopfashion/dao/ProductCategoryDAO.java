package com.shopfashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopfashion.entity.ProductCategory;

public interface ProductCategoryDAO extends JpaRepository<ProductCategory, Integer>{

}
