package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.ShippingType;

public interface ShippingTypeDAO extends JpaRepository<ShippingType, Integer>{

	@Query("SELECT ship FROM ShippingType ship WHERE ship.deleted=false AND ship.deleted_by IS NULL")
	List<ShippingType> getListShippingTypes();

	boolean existsByCode(String code);

}
