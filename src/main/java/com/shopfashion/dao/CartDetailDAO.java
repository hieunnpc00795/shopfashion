package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.CartDetail;

public interface CartDetailDAO extends JpaRepository<CartDetail, Integer>{

	@Query("SELECT detail FROM CartDetail detail WHERE detail.cart.id=?1 AND detail.product.id=?2 AND detail.product_variant.id=?3")
	CartDetail findByVariant(Integer cart_id, Integer pro_id, Integer variant_id);

	@Query("SELECT detail FROM CartDetail detail WHERE detail.cart.id=?1 AND detail.product.id=?2 AND detail.product_variant.id IS NULL")
	CartDetail findByProduct(Integer id, Integer id2);

	@Query("SELECT detail FROM CartDetail detail WHERE detail.cart.id=?1")
	List<CartDetail> findByCart(Integer cart_id);

	@Query("SELECT SUM(detail.qty) FROM CartDetail detail WHERE detail.cart.id=?1")
	Integer countProductsByCart(Integer id);
	
}
