package com.shopfashion.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.District;

public interface DistrictDAO extends JpaRepository<District, Integer>{

	@Query("select dt from District dt where dt.city.id=?1")
	List<District> getListDistrictsByCity(Integer cityId);

	@Query("select dt from District dt where dt.code=?1")
	District findByCode(String code);
	
}
