package com.shopfashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopfashion.entity.Cart;

public interface CartDAO extends JpaRepository<Cart, Integer>{

	@Query("SELECT cart FROM Cart cart WHERE cart.session.id=?1 AND cart.account.id IS NULL")
	Cart findBySession(Integer id);

	@Query("SELECT cart FROM Cart cart WHERE cart.account.id=?1")
	Cart findByAccount(Integer id);

}
