package com.shopfashion.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shopfashion.common.ETypeAttribute;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "attribute_groups")
public class AttributeGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String code;
	
	String name;
	
	ETypeAttribute type;
	
	Integer level;
	
	String description;
	
	Boolean activity;
	
	Boolean deleted;
	
	Integer deleted_by;
	
	@JsonIgnore
	@OneToMany(mappedBy = "attribute_group")
	List<Attribute> attributes;

	public AttributeGroup() {
		super();
	}

	public AttributeGroup(String code, String name, ETypeAttribute type, Integer level,String description, Boolean activity, Boolean deleted) {
		super();
		this.code = code;
		this.name = name;
		this.type = type;
		this.level = level;
		this.description = description;
		this.activity = activity;
		this.deleted = deleted;
	}
}
