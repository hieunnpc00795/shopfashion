package com.shopfashion.entity;

import java.io.Serializable;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "verifications")
public class Verification implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String password;
	
	String code;
	
	Date expiry;
	
	Integer type;
	
	Boolean activity;
	
	@ManyToOne
	@JoinColumn(name = "account_id")
	Account account;

	public Verification() {
		super();
	}

	public Verification(String password, String code, Date expiry, Integer type, Boolean activity, Account account) {
		super();
		this.password = password;
		this.code = code;
		this.expiry = expiry;
		this.type = type;
		this.activity = activity;
		this.account = account;
	}
}
