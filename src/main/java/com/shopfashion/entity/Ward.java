package com.shopfashion.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "wards")
public class Ward {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	private String code;
	
	private String name;
	
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "district_id")
	District district;
	
	@JsonIgnore
	@OneToMany(mappedBy = "ward")
	List<Account> accounts;
	
	@JsonIgnore
	@OneToMany(mappedBy = "ward")
	List<Cart> carts;

	public Ward() {
		super();
	}

	public Ward(Integer id, String code, String name, String type, District district) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.district = district;
	}
	
}
