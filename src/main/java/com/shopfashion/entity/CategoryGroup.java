package com.shopfashion.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "category_groups")
public class CategoryGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String code;
	
	String name;
	
	Integer level;
	
	String icon;
	
	Boolean activity;
	
	Boolean deleted;
	
	Integer deleted_by;
	
	@JsonIgnore
	@OneToMany(mappedBy = "categoryGroup", fetch = FetchType.EAGER)
	List<Category> categories;

	public CategoryGroup() {
		super();
	}

	public CategoryGroup(String code, String name, Integer level, String icon, Boolean activity, Boolean deleted) {
		super();
		this.code = code;
		this.name = name;
		this.level = level;
		this.icon = icon;
		this.activity = activity;
		this.deleted = deleted;
	}
	
}
