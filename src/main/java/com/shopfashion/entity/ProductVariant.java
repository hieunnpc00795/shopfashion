package com.shopfashion.entity;

import java.util.HashSet;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shopfashion.common.EWeightUnit;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "product_variants")
public class ProductVariant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String name;
	
	Integer price;
	
	Integer qty;
	
	Integer weight;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 50)
	EWeightUnit weight_unit;
	
	Integer sold_count;
	
	String image;
	
	Boolean activity;
	
	Boolean deleted;
	
	Integer deleted_by;
	
	@ManyToOne 
	@JoinColumn(name = "product_id")
	Product product;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "product_attributes", joinColumns = @JoinColumn(name = "product_variant_id"), 
			inverseJoinColumns = @JoinColumn(name = "attribute_id"))
	Set<Attribute> attributes = new HashSet<Attribute>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "product_variant_attribute", fetch = FetchType.LAZY)
	List<ProductAttribute> product_attributes;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product_variant", fetch = FetchType.LAZY)
	List<PriceHistory> price_histories;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product_variant")
	List<CartDetail> cart_details;

	public ProductVariant() {
		super();
	}

	public ProductVariant(String name, Integer price, Integer qty, Integer weight, EWeightUnit weight_unit,
			Integer sold_count, String image, Boolean activity, Boolean deleted, Integer deleted_by, Product product) {
		super();
		this.name = name;
		this.price = price;
		this.qty = qty;
		this.weight = weight;
		this.weight_unit = weight_unit;
		this.sold_count = sold_count;
		this.image = image;
		this.activity = activity;
		this.deleted = deleted;
		this.deleted_by = deleted_by;
		this.product = product;
	}
	
}
