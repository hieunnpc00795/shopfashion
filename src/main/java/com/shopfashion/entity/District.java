package com.shopfashion.entity;

import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "districts")
public class District {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	private String code;
	
	private String name;
	
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "city_id")
	City city;
	
	@JsonIgnore
	@OneToMany(mappedBy = "district")
	List<Ward> wards;
	
	@JsonIgnore
	@OneToMany(mappedBy = "district")
	List<Account> accounts;
	
	@JsonIgnore
	@OneToMany(mappedBy = "district")
	List<Cart> carts;

	public District() {
		super();
	}

	public District(Integer id, String code, String name, String type, City city) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.city = city;
	}
	
}
