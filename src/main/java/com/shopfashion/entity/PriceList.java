package com.shopfashion.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "price_lists")
public class PriceList {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String name;
	
	Integer cod_fee;
	
	Integer commodity_value;
	
	Float bigger;
	
	Integer type_bigger;
	
	Float less_than;
	
	Integer type_less_than;
	
	Boolean activity;
	
	Boolean deleted;
	
	Integer deleted_by;
	
	@ManyToOne
	@JoinColumn(name = "shipping_type_id")
	ShippingType shipping_type;
	
	@JsonIgnore
	@OneToMany(mappedBy = "price_list")
	List<SpecialCity> special_cities;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "special_cities", joinColumns = @JoinColumn(name = "price_list_id"), 
			inverseJoinColumns = @JoinColumn(name = "city_id"))
	private Set<City> cities = new HashSet<City>();

	public PriceList() {
		super();
	}

	public PriceList(String name, Integer cod_fee, Integer commodity_value, Float bigger, Integer type_bigger,
			Float less_than, Integer type_less_than, Boolean activity, Boolean deleted, Integer deleted_by,
			ShippingType shipping_type, Set<City> cities) {
		super();
		this.name = name;
		this.cod_fee = cod_fee;
		this.commodity_value = commodity_value;
		this.bigger = bigger;
		this.type_bigger = type_bigger;
		this.less_than = less_than;
		this.type_less_than = type_less_than;
		this.activity = activity;
		this.deleted = deleted;
		this.deleted_by = deleted_by;
		this.shipping_type = shipping_type;
		this.cities = cities;
	}
}
