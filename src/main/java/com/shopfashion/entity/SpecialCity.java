package com.shopfashion.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "special_cities")
public class SpecialCity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	@ManyToOne 
	@JoinColumn(name = "price_list_id")
	PriceList price_list;
	
	@ManyToOne 
	@JoinColumn(name = "city_id")
	City city;
	
}
