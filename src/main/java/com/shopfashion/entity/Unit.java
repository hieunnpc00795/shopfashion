package com.shopfashion.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "units")
public class Unit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;

	String code;

	String name;
	
	@JsonIgnore
	@OneToMany(mappedBy = "unit")
	List<Product> products;

	public Unit() {
		super();
	}

	public Unit(Integer id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}

}
