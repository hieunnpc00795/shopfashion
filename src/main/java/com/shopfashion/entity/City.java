package com.shopfashion.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Entity
@Data
@Table(name = "cities")
public class City {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	private String code;
	
	private String name;
	
	private String type;
	
	private Integer region;
	
	private Integer region_ghn;

	@JsonIgnore
	@OneToMany(mappedBy = "city")
	List<District> districts;
	
	@JsonIgnore
	@OneToMany(mappedBy = "city")
	List<Account> accounts;
	
	@JsonIgnore
	@OneToMany(mappedBy = "city")
	List<Cart> carts;
	
	@JsonIgnore
	@OneToMany(mappedBy = "city")
	List<SpecialCity> special_cities;
	
	public City(Integer id, String code, String name, String type) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
	}

	public City() {
		super();
	}
	
}
