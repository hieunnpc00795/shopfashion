package com.shopfashion.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "shipping_types")
public class ShippingType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	String code;
	
	String name;
	
	String logo;
	
	Boolean activity;
	
	String description;
	
	Boolean deleted;
	
	Integer deleted_by;
	
	@JsonIgnore
	@OneToMany(mappedBy = "shipping_type", fetch = FetchType.EAGER)
	List<PriceList> price_lists;

	public ShippingType() {
		super();
	}

	public ShippingType(String code, String name, String logo, Boolean activity, String description, Boolean deleted,
			Integer deleted_by) {
		super();
		this.code = code;
		this.name = name;
		this.logo = logo;
		this.activity = activity;
		this.description = description;
		this.deleted = deleted;
		this.deleted_by = deleted_by;
	}
	
}
