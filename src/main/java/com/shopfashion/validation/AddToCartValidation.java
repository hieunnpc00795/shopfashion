package com.shopfashion.validation;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.shopfashion.entity.ProductVariant;

import lombok.Data;

@Data
public class AddToCartValidation {

	@NotNull(message = "Session_id không được để trống.")
	String session_id;
	
	String token;
	
	@NotNull(message = "Mã sản phẩm không được để trống.")
	Integer product_id;
	
	List<ProductVariant> product_variants;
	
	@NotNull(message = "Số lượng sản phẩm không được để trống.")
	@Pattern(regexp = "[1-9]+", message = "Số lượng sản phẩm không hợp lệ.")
	@Length(min = 1, message = "Số lượng sản phẩm phải từ 1 số.")
	String qty;
	
}
