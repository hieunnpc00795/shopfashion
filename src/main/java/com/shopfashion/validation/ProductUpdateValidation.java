package com.shopfashion.validation;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.shopfashion.common.ETypeProduct;
import com.shopfashion.common.EWeightUnit;
import com.shopfashion.entity.Brand;
import com.shopfashion.entity.Category;
import com.shopfashion.entity.ProductImage;
import com.shopfashion.entity.Unit;

import lombok.Data;

@Data
public class ProductUpdateValidation {

	@NotNull(message = "ID sản phẩm không được để trống.")
	Integer id;
	
	@NotNull(message = "Vui lòng nhập mã sản phẩm.")
	String code;
	
	@NotNull(message = "Vui lòng nhập tên sản phẩm.")
	String name;
	
	@NotNull(message = "Vui lòng chọn loại cho sản phẩm.")
	ETypeProduct type;
	
//	@NotNull(message = "Vui lòng nhập giá sản phẩm.")
//	@Pattern(regexp = "[0-9]+", message = "Giá sản phẩm không hợp lệ.")
//	@Length(min = 1, message = "Giá sản phẩm phải từ 1 số.")
//	String price;
	
	@NotNull(message = "Vui lòng nhập mô tả ngắn sản phẩm.")
	String short_description;
	
	@NotNull(message = "Vui lòng nhập mô tả sản phẩm.")
	String description;
	
	String size_chart;
	
	@NotNull(message = "Vui lòng nhập trọng lượng sản phẩm.")
	@Pattern(regexp = "[0-9]+", message = "Trọng lượng sản phẩm không hợp lệ.")
	@Length(min = 1, message = "Trọng lượng sản phẩm phải từ 1 số.")
	String weight;
	
	@NotNull(message = "Vui lòng chọn đơn vị trọng lượng cho sản phẩm.")
	EWeightUnit weight_unit;
	
	@NotNull(message = "Vui lòng chọn trạng thái cho sản phẩm.")
	Boolean activity;
	
	@NotNull(message = "Vui lòng chọn thương hiệu cho sản phẩm.")
	Brand brand;
	
	@NotNull(message = "Vui lòng chọn đơn vị tính cho sản phẩm.")
	Unit unit;
	
	@NotNull(message = "Vui lòng chọn danh mục sản phẩm.")
	@Size(min = 1, message = "Vui lòng chọn danh mục sản phẩm.")
	Set<Category> categories;
	
	@NotNull(message = "Vui lòng chọn hình ảnh sản phẩm.")
	@Size(min = 1, message = "Vui lòng chọn hình ảnh sản phẩm.")
	List<ProductImage> images;
	
}
