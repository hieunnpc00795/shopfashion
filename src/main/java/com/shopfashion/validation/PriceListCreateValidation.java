package com.shopfashion.validation;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.shopfashion.entity.City;
import com.shopfashion.entity.ShippingType;

import lombok.Data;

@Data
public class PriceListCreateValidation {

	@NotNull(message = "Vui lòng nhập tên bảng giá.")
	String name;

	@NotNull(message = "Vui lòng nhập giá phí dịch vụ COD.")
	@Pattern(regexp = "[0-9]+", message = "Giá phí dịch vụ COD không hợp lệ.")
	@Length(min = 1, message = "Giá phí dịch vụ COD phải từ 1 số.")
	String cod_fee;
	
	@NotNull(message = "Vui lòng nhập giá trị hàng hóa.")
	@Pattern(regexp = "[0-9]+", message = "Giá trị hàng hóa không hợp lệ.")
	@Length(min = 1, message = "Giá trị hàng hóa phải từ 1 số.")
	String commodity_value;
	
	@NotNull(message = "Vui lòng nhập phí giá trị hàng hóa khi lớn hơn.")
	@Length(min = 1, message = "Phí trị hàng hóa phải từ 1 số.")
	String bigger;
	
	@NotNull(message = "Vui lòng chọn kiểu phí giá trị hàng hóa.")
	Integer type_bigger;
	
	@NotNull(message = "Vui lòng nhập phí giá trị hàng hóa khi nhỏ hơn.")
	@Length(min = 1, message = "Phí trị hàng hóa phải từ 1 số.")
	String less_than;
	
	@NotNull(message = "Vui lòng chọn kiểu phí giá trị hàng hóa.")
	Integer type_less_than;
	
	@NotNull(message = "Vui lòng chọn trạng thái hoạt động.")
	Boolean activity;
	
	@NotNull(message = "Vui lòng chọn đơn vị vận chuyển.")
	ShippingType shipping_type;
	
	Set<City> cities;
}
