package com.shopfashion.validation;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class SigninValidation {

	@NotNull(message = "Vui lòng nhập tên đăng nhập.")
	private String username;
	
	@NotNull(message = "Vui lòng nhập mật khẩu.")
	private String password;
	
}
