package com.shopfashion.validation;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.shopfashion.common.EWeightUnit;
import com.shopfashion.entity.Attribute;
import com.shopfashion.entity.Product;

import lombok.Data;

@Data
public class ProductVariantUpdateValidation {

	@NotNull(message = "ID biến thể không được để trống.")
	Integer id;
	
	@NotNull(message = "Vui lòng nhập tên biến thể.")
	String name;
	
	@NotNull(message = "Vui lòng nhập trọng lượng biến thể.")
	@Pattern(regexp = "[0-9]+", message = "Trọng lượng biến thể không hợp lệ.")
	@Length(min = 1, message = "Trọng lượng biến thể phải từ 1 số.")
	String weight;
	
	@NotNull(message = "Vui lòng chọn đơn vị trọng lượng cho biến thể.")
	EWeightUnit weight_unit;
	
	@NotNull(message = "Vui lòng chọn hình ảnh cho biến thể.")
	String image;
	
	@NotNull(message = "Vui lòng chọn trạng thái cho biến thể.")
	Boolean activity;
	
	@NotNull(message = "Vui lòng chọn sản phẩm để tạo biến thể.")
	Product product;
	
	@NotNull(message = "Vui lòng chọn thuộc tính cho biến thể.")
	@Size(min = 1, message = "Vui lòng chọn thuộc tính cho biến thể.")
	Set<Attribute> attributes;
}
