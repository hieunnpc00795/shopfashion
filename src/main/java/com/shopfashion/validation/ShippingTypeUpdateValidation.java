package com.shopfashion.validation;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ShippingTypeUpdateValidation {

	@NotNull(message = "ID đơn vị không được để trống.")
	Integer id;
	
	@NotNull(message = "Vui lòng nhập mã đơn vị.")
	String code;
	
	@NotNull(message = "Vui lòng nhập tên tên đơn vị.")
	String name;
	
	@NotNull(message = "Vui lòng chọn logo cho đơn vị.")
	String logo;
	
	@NotNull(message = "Vui lòng chọn trạng thái cho đơn vị.")
	Boolean activity;
	
	@NotNull(message = "Vui lòng nhập mô tả đơn vị.")
	String description;
	
}
