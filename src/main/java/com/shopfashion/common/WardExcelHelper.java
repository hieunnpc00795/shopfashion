package com.shopfashion.common;

import java.io.IOException;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.dto.WardRequest;

public class WardExcelHelper {
	
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "id", "code", "name", "district_code" };
	static String SHEET = "wards";
	
	public static boolean hasExcelFormat(MultipartFile file) {
	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }
	    return true;
	}
	
	public static List<WardRequest> excelWards(InputStream is) {
	    try {
	      Workbook workbook = new XSSFWorkbook(is);
	      Sheet sheet = workbook.getSheet(SHEET);
	      Iterator<Row> rows = sheet.iterator();
	      List<WardRequest> wards = new ArrayList<WardRequest>();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        Iterator<Cell> cellsInRow = currentRow.iterator();
	        WardRequest ward = new WardRequest();
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	          Cell currentCell = cellsInRow.next();
	          switch (cellIdx) {
	          case 0:
	        	ward.setId((int) currentCell.getNumericCellValue());
	            break;
	          case 1:
	        	  ward.setCode(currentCell.getStringCellValue());
	            break;
	          case 2:
	        	ward.setName(currentCell.getStringCellValue());
	        	String[] types = currentCell.getStringCellValue().split("\\s");
	        	String type = null;
	        	if(types[0].equals("Xã")) {
	        		type = "Xã";
	        	}
	        	if(types[0].equals("Thị")) {
	        		type = "Thị trấn";
	        	}
	        	if(types[0].equals("Phường")) {
	        		type = "Phường";
	        	}
	        	ward.setType(type);
	            break;
	          case 3:
	        	ward.setDistrict_code(currentCell.getStringCellValue());
	            break;
	          default:
	            break;
	          }
	          cellIdx++;
	        }
	        if(ward.getId() != null) {
	        	wards.add(ward);
	        }
	      }
	      workbook.close();
	      return wards;
	    } catch (IOException e) {
	      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
	    }
	  }
	
}
