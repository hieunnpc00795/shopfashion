package com.shopfashion.common;

import java.io.IOException;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.shopfashion.dao.CityDAO;
import com.shopfashion.dto.DistrictRequest;
import com.shopfashion.entity.City;
import com.shopfashion.entity.District;

public class DistrictExcelHelper {
	
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "id", "code", "name", "city_code" };
	static String SHEET = "districts";
	
	public static boolean hasExcelFormat(MultipartFile file) {
	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }
	    return true;
	}
	
	public static List<DistrictRequest> excelDistricts(InputStream is) {
	    try {
	      Workbook workbook = new XSSFWorkbook(is);
	      Sheet sheet = workbook.getSheet(SHEET);
	      Iterator<Row> rows = sheet.iterator();
	      List<DistrictRequest> districts = new ArrayList<DistrictRequest>();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        Iterator<Cell> cellsInRow = currentRow.iterator();
	        DistrictRequest district = new DistrictRequest();
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	          Cell currentCell = cellsInRow.next();
	          switch (cellIdx) {
	          case 0:
	        	district.setId((int) currentCell.getNumericCellValue());
	            break;
	          case 1:
	        	district.setCode(currentCell.getStringCellValue());
	            break;
	          case 2:
	        	district.setName(currentCell.getStringCellValue());
	        	String[] types = currentCell.getStringCellValue().split("\\s");
	        	String type = null;
	        	if(types[0].equals("Quận")) {
	        		type = "Quận";
	        	}
	        	if(types[0].equals("Huyện")) {
	        		type = "Huyện";
	        	}
	        	if(types[0].equals("Thị")) {
	        		type = "Thị xã";
	        	}
	        	if(types[0].equals("Thành")) {
	        		type = "Thành phố";
	        	}
	        	district.setType(type);
	            break;
	          case 3:
	        	district.setCity_code(currentCell.getStringCellValue());
	            break;
	          default:
	            break;
	          }
	          cellIdx++;
	        }
	        districts.add(district);
	      }
	      workbook.close();
	      return districts;
	    } catch (IOException e) {
	      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
	    }
	  }
	
}
