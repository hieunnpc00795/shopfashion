package com.shopfashion.common;

public enum ETypeAttribute {
	
	TYPE_COLOR,
	TYPE_IMAGE,
	TYPE_LABEL
}
