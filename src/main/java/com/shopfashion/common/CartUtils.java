package com.shopfashion.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.shopfashion.dao.CartDAO;
import com.shopfashion.dao.CartDetailDAO;
import com.shopfashion.dao.ProductDAO;
import com.shopfashion.dao.ProductVariantDAO;
import com.shopfashion.entity.Account;
import com.shopfashion.entity.Cart;
import com.shopfashion.entity.CartDetail;
import com.shopfashion.entity.Product;
import com.shopfashion.entity.ProductVariant;
import com.shopfashion.entity.Session;
import com.shopfashion.message.ResponseMessage;
import com.shopfashion.message.ResponseMessageError;
import com.shopfashion.validation.AddToCartValidation;

@Component
public class CartUtils {

	@Autowired
	CartDAO cartDAO;
	
	@Autowired
	CartDetailDAO cartDetailDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	ProductVariantDAO productVariantDAO;
	
	public void mergeClientCartToUserCart(Account account, Session session){
		Cart client_cart = cartDAO.findBySession(session.getId());
		Cart user_cart = cartDAO.findByAccount(account.getId());
		if(client_cart != null && user_cart != null) {
			List<CartDetail> client_cart_details = cartDetailDAO.findByCart(client_cart.getId());
			for(CartDetail client_cart_detail:client_cart_details) {
				if(client_cart_detail.getProduct_variant() != null) {
					CartDetail user_cart_detail = cartDetailDAO.findByVariant(user_cart.getId(), client_cart_detail.getProduct().getId(), client_cart_detail.getProduct_variant().getId());
					if(user_cart_detail != null) {
						Integer qty = user_cart_detail.getQty()+client_cart_detail.getQty();
						Integer total = qty * user_cart_detail.getPrice();
						user_cart_detail.setQty(qty);
						user_cart_detail.setTotal(total);
						cartDetailDAO.save(user_cart_detail);
						cartDetailDAO.delete(client_cart_detail);
					} else {
						client_cart_detail.setCart(user_cart);
						cartDetailDAO.save(client_cart_detail);
					}
				} else {
					CartDetail user_cart_detail = cartDetailDAO.findByProduct(user_cart.getId(), client_cart_detail.getProduct().getId());
					if(user_cart_detail != null) {
						Integer qty = user_cart_detail.getQty() + client_cart_detail.getQty();
						Integer total = qty * user_cart_detail.getPrice();
						user_cart_detail.setQty(qty);
						user_cart_detail.setTotal(total);
						cartDetailDAO.save(user_cart_detail);
						cartDetailDAO.delete(client_cart_detail);
					} else {
						client_cart_detail.setCart(user_cart);
						cartDetailDAO.save(client_cart_detail);
					}
				}
			}
			cartDAO.delete(client_cart);
		}
		if(client_cart != null && user_cart == null) {
			client_cart.setAccount(account);
			cartDAO.save(client_cart);
		}
	}
	
	public ResponseEntity<?> clientAddToCart(AddToCartValidation addToCartValidation, Session session) {
		Product product = productDAO.findById(addToCartValidation.getProduct_id()).get();
		if(product == null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Sản phẩm không tồn tại!", "product"));
		}
		Integer sum_variant = productVariantDAO.sumVariantByProduct(product.getId());
		ProductVariant product_variant = null;
		if(sum_variant > 0) {
			if(addToCartValidation.getProduct_variants().size() > 1 || addToCartValidation.getProduct_variants().size() < 1) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn phân loại hàng!", "product_variant"));
			}
			product_variant = productVariantDAO.findVariantByIDAndProductID(product.getId(), addToCartValidation.getProduct_variants().get(0).getId());
			if(product_variant == null) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Phân loại hàng không tồn tại!", "product_variant"));
			}
		}
		Cart cart = cartDAO.findBySession(session.getId());
		if(cart == null) {
			Cart param = new Cart(session);
			cartDAO.save(param);
			Integer qty = Integer.parseInt(addToCartValidation.getQty());
			Integer price = 0;
			if(product_variant != null) {
				price = product_variant.getPrice();
			} else {
				price = product.getPrice();
			}
			Integer total = qty * price;
			CartDetail cart_detail = new CartDetail(qty,
					price,
					total,
					param,
					product,
					product_variant);
			cartDetailDAO.save(cart_detail);
		} else {
			Integer qty = Integer.parseInt(addToCartValidation.getQty());
			Integer price = 0;
			Integer total = 0;
			if(product_variant != null) {
				CartDetail cart_detail = cartDetailDAO.findByVariant(cart.getId(), product.getId(), product_variant.getId());
				price = product_variant.getPrice();
				if(cart_detail != null) {
					qty += cart_detail.getQty();
					total = qty * price;
					cart_detail.setQty(qty);
					cart_detail.setPrice(price);
					cart_detail.setTotal(total);
					cartDetailDAO.save(cart_detail);
				} else {
					total = qty * price;
					cart_detail = new CartDetail(qty,
							price,
							total,
							cart,
							product,
							product_variant);
					cartDetailDAO.save(cart_detail);
				}
			} else {
				CartDetail cart_detail = cartDetailDAO.findByProduct(cart.getId(), product.getId());
				price = product.getPrice();
				if(cart_detail != null) {
					qty+= cart_detail.getQty();
					total = qty * price;
					cart_detail.setQty(qty);
					cart_detail.setPrice(price);
					cart_detail.setTotal(total);
					cartDetailDAO.save(cart_detail);
				} else {
					total = qty * price;
					cart_detail = new CartDetail(qty,
							price,
							total,
							cart,
							product,
							null);
					cartDetailDAO.save(cart_detail);
				}
			}
		}
		return ResponseEntity.ok(new ResponseMessage("Sản phẩm đã được thêm vào giỏ hàng thành công!"));
	}
	
	public ResponseEntity<?> userAddToCart(AddToCartValidation addToCartValidation, Account account, Session session){
		Product product = productDAO.findById(addToCartValidation.getProduct_id()).get();
		if(product == null) {
			return ResponseEntity.badRequest().body(new ResponseMessageError("Sản phẩm không tồn tại!", "product"));
		}
		Integer sum_variant = productVariantDAO.sumVariantByProduct(product.getId());
		ProductVariant product_variant = null;
		if(sum_variant > 0) {
			if(addToCartValidation.getProduct_variants().size() > 1 || addToCartValidation.getProduct_variants().size() < 1) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Vui lòng chọn phân loại hàng!", "product_variant"));
			}
			product_variant = productVariantDAO.findVariantByIDAndProductID(product.getId(), addToCartValidation.getProduct_variants().get(0).getId());
			if(product_variant == null) {
				return ResponseEntity.badRequest().body(new ResponseMessageError("Phân loại hàng không tồn tại!", "product_variant"));
			}
		}
		Cart cart = cartDAO.findByAccount(account.getId());
		if(cart == null) {
			cart = new Cart(account, session);
			cartDAO.save(cart);
			Integer qty = Integer.parseInt(addToCartValidation.getQty());
			Integer price = 0;
			if(product_variant != null) {
				price = product_variant.getPrice();
			} else {
				price = product.getPrice();
			}
			Integer total = qty * price;
			CartDetail cart_detail = new CartDetail(qty,
					price,
					total,
					cart,
					product,
					product_variant);
			cartDetailDAO.save(cart_detail);
		} else {
			Integer qty = Integer.parseInt(addToCartValidation.getQty());
			Integer price = 0;
			Integer total = 0;
			if(product_variant != null) {
				CartDetail cart_detail = cartDetailDAO.findByVariant(cart.getId(), product.getId(), product_variant.getId());
				price = product_variant.getPrice();
				if(cart_detail != null) {
					qty += cart_detail.getQty();
					total = qty * price;
					cart_detail.setQty(qty);
					cart_detail.setPrice(price);
					cart_detail.setTotal(total);
					cartDetailDAO.save(cart_detail);
				} else {
					total = qty * price;
					cart_detail = new CartDetail(qty,
							price,
							total,
							cart,
							product,
							product_variant);
					cartDetailDAO.save(cart_detail);
				}
			} else {
				CartDetail cart_detail = cartDetailDAO.findByProduct(cart.getId(), product.getId());
				price = product.getPrice();
				if(cart_detail != null) {
					qty+= cart_detail.getQty();
					total = qty * price;
					cart_detail.setQty(qty);
					cart_detail.setPrice(price);
					cart_detail.setTotal(total);
					cartDetailDAO.save(cart_detail);
				} else {
					total = qty * price;
					cart_detail = new CartDetail(qty,
							price,
							total,
							cart,
							product,
							null);
					cartDetailDAO.save(cart_detail);
				}
			}
		}
		return ResponseEntity.ok(new ResponseMessage("Sản phẩm đã được thêm vào giỏ hàng thành công!"));
	}
	
}
