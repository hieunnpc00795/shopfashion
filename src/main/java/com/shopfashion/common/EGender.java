package com.shopfashion.common;

public enum EGender {
	MALE,
	FEMALE,
	OTHER
}
