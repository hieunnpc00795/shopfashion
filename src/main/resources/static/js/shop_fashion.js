const app = angular.module("shop-fashion-app",[]);
app.controller("shop-fashion-ctrl" , function($scope , $http){
	
	$scope.category_groups = [];
	$scope.list_attributes_variant = [];
	$scope.attribute_responses = [];
	$scope.variants = [];
	$scope.totalProductsInCart = 2;
	$scope.cart= {};
	
	$scope.getInit = function(){
		$scope.getSession();
		
		$scope.category_group();
		
		$scope.totalProductsInCart();
		
		$scope.mergeClientCartToUserCart();
		
		$scope.cart_detail();
	};
	
	$scope.getSession = function(){
		var session_id = localStorage.getItem("session_id");
		var session = session_id?JSON.parse(session_id):null;
		if(session == null){
			$http.get("/client/session").then(resp => {
				session = JSON.stringify(resp.data.session_id);
				localStorage.setItem("session_id", session);
			});
		}
	}
	
	$scope.category_group = function(){
		$http.get("/rest/category-groups").then(resp => {
			$scope.category_groups = resp.data;
			$scope.category_groups.forEach(category_group => {
				$scope.findCategoriesByCategoryGroup(category_group);
			});
		});	
	}
	
	$scope.findCategoriesByCategoryGroup = function(category_group){
		$http.get(`/rest/categories/find-categories-by-category-group/${category_group.id}`).then(resp => {
			category_group.categories = resp.data;
		});
	}
	
	$scope.findAttribute = function(product_id, attribute_id, level){
		/* var url = location.href;
		window.location.href = url; */
		$scope.attribute_responses.forEach(attribute_response =>{
			if(attribute_response.id > level){
				var index = $scope.attribute_responses.findIndex(attribute => attribute_response.id==attribute.id);
				$scope.attribute_responses.splice(index, 1);	
			}
		});
		var attribute_variant = {
			id: level,
			attribute_id: attribute_id
		}
		if($scope.list_attributes_variant.length > 0){
			var index_first = $scope.list_attributes_variant.findIndex(attribute => attribute.id === level);
			if(index_first >= 0){
				$scope.list_attributes_variant[index_first] = attribute_variant;
				var index_two = $scope.list_attributes_variant.findIndex(attribute => attribute.id > level);
				if(index_two >= 0){
					$scope.list_attributes_variant.splice(index_two, 1);
				}
			} else{
				$scope.list_attributes_variant.push(attribute_variant);
			}
		} else {
			$scope.list_attributes_variant.push(attribute_variant);
		}
		$scope.list_attributes_variant.forEach(attribute_variant =>{
			if(attribute_variant.id > level){
				var index_3 = $scope.list_attributes_variant.findIndex(attribute => attribute_variant.id==attribute.id);
				$scope.list_attributes_variant.splice(index_3, 1);	
			}
		});
		var attribute_request = {
			product_id: product_id,
			attributes: $scope.list_attributes_variant
		}
		$scope.getVariantsByAttributes(attribute_request);
		$http.post(`/rest/product-attributes/${attribute_id}/${level}`, attribute_request).then(resp => {
			$scope.attribute_responses.forEach(attribute_response =>{
				if(attribute_response.id > level){
					var index = $scope.attribute_responses.findIndex(attribute => attribute_response.id==attribute.id);
					$scope.attribute_responses.splice(index, 1);	
				}
			});
			if(resp.data.id){
				$scope.attribute_responses.push(resp.data);
			}
		});
	}
	
	$scope.getVariantsByAttributes = function(attribute_request){
		$scope.variants = [];
		$http.post(`/rest/product-variants/get-variants`, attribute_request).then(resp => {
			$scope.variants = resp.data;
		}).catch(error => {
			$scope.error = error.data;
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: ''
			})
		})
	}
	
	$scope.totalProductsInCart = function(){
		var session_id = localStorage.getItem("session_id");
		session_id = session_id?JSON.parse(session_id):null;
		
		var token = localStorage.getItem("token");
		token = token?JSON.parse(token):null;
		
		var cart_request = {
			session_id: session_id,
			token: token
		}
		
		$http.post(`/rest/carts`, cart_request).then(resp => {
			$scope.totalProductsInCart = resp.data;
			console.log($scope.totalProductsInCart);
		}).catch(error => {
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: error.data.message,
			  footer: ''
			})
		})
	}
	
	$scope.mergeClientCartToUserCart = function(){
		var session_id = localStorage.getItem("session_id");
		session_id = session_id?JSON.parse(session_id):null;
		
		var token = localStorage.getItem("token");
		token = token?JSON.parse(token):null;
		if(token != null && session_id != null){
			var cart_request = {
				session_id: session_id,
				token: token
			}
			$http.post(`/rest/carts/merge-cart`, cart_request).then(resp => {
				
			})	
		}
	}
	
	$scope.cart_detail = function(){
		var session_id = localStorage.getItem("session_id");
		session_id = session_id?JSON.parse(session_id):null;
		
		var token = localStorage.getItem("token");
		token = token?JSON.parse(token):null;
		
		$http.get(`/rest/carts/${session_id}/${token}`).then(resp => {
			$scope.cart = resp.data;
			$scope.cart.cart_details.forEach(cart_detail => {
				$scope.findImagesByProduct(cart_detail.product);
			});
			console.log($scope.cart);
		}).catch(error => {
			console.log(error);
		})
	}
	
	$scope.addToCart = function(product_id, qty){
		var session_id = localStorage.getItem("session_id");
		session_id = session_id?JSON.parse(session_id):null;
		var cart = {
			session_id: session_id,
			token: null,
			product_id: product_id,
			product_variants: $scope.variants,
			qty: qty
		}
		$http.post(`/rest/carts/add-to-cart`, cart).then(resp => {
			$scope.totalProductsInCart++;
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: resp.data.message,
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: error.data.message,
			  footer: ''
			})
		})
	}
	
	$scope.findImagesByProduct = function(product){
		$http.get(`/rest/product-images/find-images-by-product/${product.id}`).then(resp => {
			product.product_images = resp.data;
		});
	}
	
	$scope.getInit();
	
})