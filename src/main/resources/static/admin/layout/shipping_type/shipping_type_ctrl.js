app.controller("shipping-type-ctrl" , function($scope ,$http){
	
	$scope.form = {};
	$scope.shipping_types = [];
	$scope.error = {};
	
	$scope.initialize = function(){
		$scope.form.logo = 'mailchimp.png';
		
		$http.get("/rest/shipping-types").then(resp => {
			$scope.shipping_types = resp.data;
		});
	}
	
	$scope.create = function(){
		var shipping_type = angular.copy($scope.form);
		$scope.error = {};
		$http.post(`/rest/shipping-types`, shipping_type).then(resp => {
			$scope.reset();
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: resp.data.message,
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			$scope.error = error.data;
			if($scope.error.type == 'logo'){
				$scope.error.logo = $scope.error.message;
			}
			if($scope.error.type == 'code'){
				$scope.error.code = $scope.error.message;
			}
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: ''
			})
		})
	}
	
	$scope.reset = function(){
		$scope.form = {};
		$scope.shipping_types = [];
		$scope.error = {};
		$scope.initialize();
	}
	
	$scope.update = function(){
		var shipping_type = angular.copy($scope.form);
		$scope.error = {};
		$http.put(`/rest/shipping-types`, shipping_type).then(resp => {
			$scope.reset();
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: resp.data.message,
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			$scope.error = error.data;
			if($scope.error.type == 'logo'){
				$scope.error.icon = $scope.error.message;
			}
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: ''
			})
		})
	}
	
	$scope.delete = function(shipping_type){
		Swal.fire({  
		  title: 'Bạn có chắc chắn xóa đơn vị này?',  
		  showCancelButton: true,
		  cancelButtonText: `Hủy`,  
		  confirmButtonText: `Xác nhận`,  
		}).then((result) => {  
			/* Read more about isConfirmed, isDenied below */  
		    if (result.isConfirmed) {
				$scope.error = {};
				$http.put(`/rest/shipping-types/${shipping_type.id}`, shipping_type).then(resp=>{
					$scope.reset();
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: resp.data.message,
						showConfirmButton: false,
						timer: 2500
					})
				}).catch(error=>{
					$scope.error = error.data;
					Swal.fire({
					  icon: 'error',
					  title: 'LỖI',
					  text: 'Đã xảy ra sự cố!',
					  footer: ''
					})
				})    
		    	/* Swal.fire('Saved!', '', 'success') */ 
		    } else {    
		    	Swal.fire('Bạn đã hủy việc xóa!', '', 'info')  
		 	}
		});
	}
	
	$scope.edit = function(shipping_type){
		$scope.form = angular.copy(shipping_type);
	}
	
	$scope.iconChanged = function(files){
		var data = new FormData();
		data.append('file', files[0]);
		$http.post('/rest/upload/shipping_types', data, {
			transformRequest:angular.identity,
			headers:{'Content-Type': undefined}
		}).then(resp => {
			$scope.form.logo=resp.data.name;
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Tải hình ảnh thành công!',
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: error
			})
		})
	}
	
	$scope.resetIcon = function() {
		$scope.form.logo = 'mailchimp.png';
	}
	
	$scope.initialize();
});