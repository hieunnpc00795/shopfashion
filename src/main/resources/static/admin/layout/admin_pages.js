app = angular.module("admin-pages",['ngRoute', 'ng.ckeditor']);

app.config(function($routeProvider){
    $routeProvider
    .when("/accounts" ,{
        templateUrl :"/admin/layout/account/account_page.html",
        controller: "account-ctrl"
    })
    .when("/attributes" ,{
        templateUrl :"/admin/layout/attribute/attribute_page.html",
		 controller: "attribute-ctrl"
        
    })
    .when("/attribute_groups" ,{
        templateUrl :"/admin/layout/attribute_group/attribute_group_page.html",
		 controller: "attribute-group-ctrl"
        
    })
    .when("/batches" ,{
        templateUrl :"/admin/layout/batches/batches_page.html",
		 controller: "batches-ctrl"
        
    })
    .when("/brands" ,{
        templateUrl :"/admin/layout/brand/brand_page.html",
		 controller: "brand-ctrl"
        
    })
    .when("/categories" ,{
        templateUrl :"/admin/layout/category/category_page.html",
		 controller: "category-ctrl"
        
    })
    .when("/category_groups" ,{
        templateUrl :"/admin/layout/category_group/category_group_page.html",
		 controller: "category-group-ctrl"
        
    })
    .when("/price_lists" ,{
        templateUrl :"/admin/layout/price_list/price_list_page.html",
		 controller: "price-list-ctrl"
        
    })
    .when("/products" ,{
        templateUrl :"/admin/layout/product/product_page.html",
		 controller: "product-ctrl"
        
    })
    .when("/roles" ,{
        templateUrl :"/admin/layout/role/role_page.html",
		 controller: "role-ctrl"
        
    })
    .when("/shipping_types" ,{
        templateUrl :"/admin/layout/shipping_type/shipping_type_page.html",
		 controller: "shipping-type-ctrl"
        
    })
    .when("/warehouses" ,{
        templateUrl :"/admin/layout/warehouse/warehouse_page.html",
		 controller: "warehouse-ctrl"
        
    })
    .when("/warehouse_details" ,{
        templateUrl :"/admin/layout/warehouse_detail/warehouse_detail_page.html",
		 controller: "warehouse-detail-ctrl"
        
    })
    .otherwise({
        template :"<h1 class='text-center' ></h1>"
    });
})