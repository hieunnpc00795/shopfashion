app.controller("role-ctrl" , function($scope ,$http){
	
	$scope.roles = [];
	
	$scope.getInit = function(){
		$http.get("/rest/roles").then(resp =>{
             $scope.roles = resp.data;
        });
	}
	
	$scope.getInit();
});