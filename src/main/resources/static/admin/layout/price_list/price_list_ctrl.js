app.controller("price-list-ctrl" , function($scope ,$http){
	
	$scope.form = {};
	$scope.error = {};
	$scope.shipping_types = [];
	$scope.price_lists = [];
	$scope.cities = [];
	$scope.city = {};
	$scope.special_cities = [];
	
	$scope.initialize = function(){
		
		$scope.form.cities = [];
		
		$http.get("/rest/shipping-types").then(resp => {
			$scope.shipping_types = resp.data;
		});
		
		$http.get("/rest/cities").then(resp => {
			$scope.cities = resp.data;
		});
		
		$http.get("/rest/price-lists").then(resp => {
			$scope.price_lists = resp.data;
		});
	};
	
	$scope.create = function(){
		var price_list = angular.copy($scope.form);
		$scope.error = {};
		$http.post(`/rest/price-lists`, price_list).then(resp => {
			$scope.reset();
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: resp.data.message,
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			$scope.error = error.data
			if(error.data.type === 'price_list'){
				Swal.fire({
				  icon: 'error',
				  title: 'LỖI',
				  text: error.data.message,
				  footer: ''
				})
			}
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: ''
			})
		});
	}
	
	$scope.update = function(){
		var price_list = angular.copy($scope.form);
		$scope.error = {};
		$http.put(`/rest/price-lists`, price_list).then(resp => {
			$scope.reset();
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: resp.data.message,
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			$scope.error = error.data
			if(error.data.type === 'price_list'){
				Swal.fire({
				  icon: 'error',
				  title: 'LỖI',
				  text: error.data.message,
				  footer: ''
				})
			}
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Đã xảy ra sự cố!',
			  footer: ''
			})
		});
	}
	
	$scope.reset = function(){
		$scope.form = {};
		$scope.form.cities = [];
		$scope.error = {};
		$scope.shipping_types = [];
		$scope.city = {};
		$scope.special_cities = [];
	}
	
	$scope.get_special_cities = function(id){
		$http.get(`/rest/special-cities/${id}`).then(resp => {
			$scope.special_cities=resp.data;
		})
	}
	
	$scope.special_cities_of = function(city){
		if($scope.form.id){
			if($scope.special_cities){
				return $scope.special_cities
				.find(special_city => special_city.price_list.id===$scope.form.id && special_city.city.id===city.id);
			}
		}
	}
	
	$scope.choose_special_cities = function(city){
		if($scope.form.id){
			var special_city = $scope.special_cities_of(city);
			if(!special_city){
				special_city = {
					price_list: $scope.form,
					city: city
				}
				$scope.grant_special_city(special_city);
				$scope.form.cities.push(city);
			}
		}
		
		if(!$scope.form.id && city.id != null){
			var check_city = $scope.form.cities.find(form_city => form_city.id===city.id);
			if(!check_city){
				$scope.form.cities.push(city);
			}
		}
	};
	
	$scope.revoke_special_city = function(city){
		if($scope.form.id){
			var special_city = $scope.special_cities_of(city);
			if(special_city){
				$http.delete(`/rest/special-cities/${special_city.id}`).then(resp => {
					$scope.get_special_cities($scope.form.id);
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: "Xóa vùng đặc biệt thành công!",
						showConfirmButton: false,
						timer: 2500
					})
				}).catch(error => {
					Swal.fire({
					  icon: 'error',
					  title: 'LỖI',
					  text: 'Xóa vùng đặc biệt thất bại!',
					  footer: error
					})
				});	
			}
		}
		
		if(!$scope.form.id){
			var check_city = $scope.form.cities.find(form_city => form_city.id===city.id);
			if(check_city){
				var index = $scope.form.cities.findIndex(form_city => form_city.id===city.id);
				$scope.form.cities.splice(index, 1);
			}
		}
	}
	
	$scope.grant_special_city = function(special_city){
		$http.post('/rest/special-cities/', special_city).then(resp => {
			$scope.get_special_cities($scope.form.id);
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Thêm vùng đặc biệt thành công!',
				showConfirmButton: false,
				timer: 2500
			})
		}).catch(error => {
			Swal.fire({
			  icon: 'error',
			  title: 'LỖI',
			  text: 'Thêm vùng đặc biệt thất bại!',
			  footer: error
			})
		});
	}
	
	$scope.initialize();
	
});